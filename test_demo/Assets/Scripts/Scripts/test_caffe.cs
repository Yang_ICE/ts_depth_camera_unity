﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;
using System;
using System.Text;

public class test_caffe : MonoBehaviour {
    // Use this for initialization
    void Start () {
        CaffeNet net = new CaffeNet();

        print("******************************");
        print("test mode and device");

        net.set_mode(0);

        if (net.check_device(0))
            print("Device 0 exists");
        else
            print("Device 0 does not exist");

        if (net.check_device(1))
            print("Device 1 exists");
        else
            print("Device 1 does not exist");

        net.set_mode(1);

        if (net.check_device(0))
            print("Device 0 exists");
        else
            print("Device 0 does not exist");

        if (net.check_device(1))
            print("Device 1 exists");
        else
            print("Device 1 does not exist");

        net.set_mode(1);
        net.set_device(0);

        print("******************************");
        print("test model and weight");

        StringBuilder model_name = new StringBuilder(1024);
        model_name.Append("C:/Code/fcn_berkeleyvision_org/voc-fcnClickPattern8s/deploy.prototxt");
        net.set_model(model_name, model_name.Length);
        print(model_name.Length);
        model_name.Remove(0, model_name.Length);

        StringBuilder name_out = new StringBuilder(1024);
        //StringBuilder name_out = new StringBuilder(10);
        net.get_model(name_out);
        print(name_out.Length);
        print(name_out.ToString());
        name_out.Remove(0, name_out.Length);

        StringBuilder weight_file_name = new StringBuilder(1024);
        weight_file_name.Append("C:/Code/fcn_berkeleyvision_org/voc-fcnClickPattern8s/snapshot/train_iter_500000.caffemodel");
        net.set_weight(weight_file_name, weight_file_name.Length);
        print("input length: "+weight_file_name.Length);
        weight_file_name.Remove(0, weight_file_name.Length);

        net.get_weight_file(name_out);
        print("output length: " + name_out.Length);
        //print("length in c++: " + len_weight_file);
        print(name_out.ToString());
        name_out.Remove(0, name_out.Length);

        print("******************************");
        print("test set input");

        int height = 0;
        int width = 0;
        int channel = 0;
        int number = 0;

        const int height_test = 281;
        const int width_test = 500;

        net.get_input_size(ref height, ref width, ref channel, ref number);

        print("Size of input before reshape: " + height + ", " + width + ", " + channel + ", " + number);

        net.input_reshape(height_test, width_test, channel, number);

        net.get_input_size(ref height, ref width, ref channel, ref number);

        print("Size of input after reshape: " + height + ", " + width + ", " + channel + ", " + number);

        net.bridge_mutable_input_data();

        StringBuilder file = new StringBuilder(500);
        file.Append("E:/Dataset/VOC2012/VOC2012/myImage/2007_000032.tiff");
        cvMat im_bgr = new cvMat(file);
        im_bgr.show(0);
        file.Remove(0, file.Length);

        file.Append("E:/Dataset/VOC2012/VOC2012/myImage/clickPattern/2007_000032_3_posClk_s1_1.png");
        cvMat im_pos_click = new cvMat(file);
        im_pos_click.show(1);
        file.Remove(0, file.Length);

        file.Append("E:/Dataset/VOC2012/VOC2012/myImage/clickPattern/2007_000032_3_negClk_s1_1.png");
        cvMat im_neg_click = new cvMat(file);
        im_neg_click.show(2);
        file.Remove(0, file.Length);

        for (int h = 0; h < height_test; h++)
        {
            for (int w = 0; w < width_test; w++)
            {
                double val = 0;

                val = im_bgr.get(h, w, 0, 16, 3) - (double)104.00698793;
                net.set_input_data(w + h * width_test, val);

                val = im_bgr.get(h, w, 1, 16, 3) - (double)116.66876762;
                net.set_input_data(w + h * width_test + height_test * width_test, val);

                val = im_bgr.get(h, w, 2, 16, 3) - (double)122.67891434;
                net.set_input_data(w + h * width_test + 2 * height_test * width_test, val);

                val = im_pos_click.get(h, w, 0, 0, 1) - (double)166.90557;
                net.set_input_data(w + h * width_test + 3 * height_test * width_test, val);

                val = im_neg_click.get(h, w, 0, 0, 1) - (double)147.47697;
                net.set_input_data(w + h * width_test + 4 * height_test * width_test, val);
            }
        }

        net.forward();

        net.bridge_output_data();

        cvMat output_DNN_0 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1 = new cvMat(height_test, width_test, 5, 0);

        for (int h = 0; h < height_test; h++)
        {
            for (int w = 0; w < width_test; w++)
            {
                double val = 0;

                val = net.get_output_data(w + h * width_test);
                output_DNN_0.set(h, w, 0, 5, 1, val);

                val = net.get_output_data(w + h * width_test + height_test * width_test);
                output_DNN_1.set(h, w, 0, 5, 1, val);

                //output_DNN_0.at<float>(h, w) = output_data[w + h * width];
                //output_DNN_1.at<float>(h, w) = output_data[w + h * width + height * width];
            }
        }

        double min = 0;
        double max = 0;

        cvMat tmp = new cvMat();
        cvMat vis = new cvMat();
        output_DNN_1.minMax(ref min, ref max);

        tmp = output_DNN_1.normalize(min, max, (double)255);

        vis = tmp.convertTo(0);
        vis.show(3);
        vis.Dispose();

        tmp.Dispose();

        cvMat output_DNN_0_neg = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1_neg = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_0_exp = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1_exp = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_0_exp_p1 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1_exp_p1 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_0_exp_p1_d1 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1_exp_p1_d1 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_sum = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_prob = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_prob_normalize = new cvMat(height_test, width_test, 5, 0);

        output_DNN_0_neg = output_DNN_0.multiply_scalar((double)(-1.0));
        output_DNN_1_neg = output_DNN_1.multiply_scalar((double)(-1.0));

        output_DNN_0_exp = output_DNN_0_neg.exp();
        output_DNN_1_exp = output_DNN_1_neg.exp();

        output_DNN_0_exp_p1 = output_DNN_0_exp.add_scalar((double)1.0);
        output_DNN_1_exp_p1 = output_DNN_1_exp.add_scalar((double)1.0);

        output_DNN_0_exp_p1_d1 = output_DNN_0_exp_p1.divide_scalar((double)1.0);
        output_DNN_1_exp_p1_d1 = output_DNN_1_exp_p1.divide_scalar((double)1.0);

        output_DNN_sum = output_DNN_0_exp_p1_d1.add(output_DNN_1_exp_p1_d1);

        output_DNN_prob = output_DNN_1_exp_p1_d1.divide(output_DNN_sum);

        output_DNN_prob.minMax(ref min, ref max);

        output_DNN_prob_normalize = output_DNN_prob.normalize(min, max, (double)255);

        output_DNN_prob_normalize.minMax(ref min, ref max);

        print("****");
        print("Min: " + min + "; Max: " + max);

        cvMat mask_0 = new cvMat();
        cvMat mask_255 = new cvMat();
        cvMat mask_n0 = new cvMat();
        cvMat mask_n255 = new cvMat();
        cvMat mask_all = new cvMat();

        mask_n0 = output_DNN_prob_normalize.isNotEqual(0);
        mask_n255 = output_DNN_prob_normalize.isNotEqual(255);
        mask_0 = output_DNN_prob_normalize.isEqual(0);
        mask_255 = output_DNN_prob_normalize.isEqual(255);
        mask_all = mask_n0.bitwise_and(mask_n255);
        output_DNN_prob_normalize.minMax_mask(ref min, ref max, mask_all);

        print("****");
        print("Min: " + min + "; Max: " + max);

        output_DNN_prob_normalize.setTo_mask((double)min, mask_0);
        output_DNN_prob_normalize.setTo_mask((double)max, mask_255);

        output_DNN_prob_normalize.minMax(ref min, ref max);

        print("****");
        print("Min: " + min + "; Max: " + max);

        vis = output_DNN_prob_normalize.convertTo(0);
        vis.show(4);
        vis.Dispose();

        print(output_DNN_prob_normalize.type());        

        output_DNN_0_neg.Dispose();
        output_DNN_1_neg.Dispose();
        output_DNN_0_exp.Dispose();
        output_DNN_1_exp.Dispose();
        output_DNN_0_exp_p1.Dispose();
        output_DNN_1_exp_p1.Dispose();
        output_DNN_0_exp_p1_d1.Dispose();
        output_DNN_1_exp_p1_d1.Dispose();
        output_DNN_sum.Dispose();
        output_DNN_prob.Dispose();

        //graph cut
        const int numNeighbour = (height_test * width_test - height_test) + (height_test * width_test - width_test) + (height_test * width_test - height_test - width_test + 1);
        const int numSite = height_test * width_test;
        const int numLabel = 2;
        const float sigma = 5;
        const float scale = 50;
        const float lambda = 1;

        //data cost
        int[] unary_data = new int[numSite * numLabel];
        for (int h = 0; h < height_test; h++)
        {
            for (int w = 0; w < width_test; w++)
            {
                float prob = (float) (output_DNN_prob_normalize.get(h, w, 0, 5, 1) / 255.0);
                unary_data[2 * (w + h * width)] = (int)((-1) * scale * lambda * Mathf.Log(1 - prob));
                unary_data[2 * (w + h * width) + 1] = (int)((-1) * scale * lambda * Mathf.Log(prob));                
            }
        }

        //smooth cost
        int[] smooth = new int[numLabel * numLabel];
        for (int i = 0; i < numLabel; i++)
        {
            for (int j = 0; j < numLabel; j++)
            {
                if (i == j)
                    smooth[i + j * numLabel] = 0;
                else
                    smooth[i + j * numLabel] = (int)scale;
            }
        }

        //neighbour cost
        print(im_bgr.type());
        cvMat img_LAB = new cvMat();
        cvMat img_LAB_FC = new cvMat();
        cvMat img_L = new cvMat();
        cvMat img_A = new cvMat();
        cvMat img_B = new cvMat();
        cvMat img_L_ori = new cvMat();
        cvMat img_A_ori = new cvMat();
        cvMat img_B_ori = new cvMat();
        img_LAB = im_bgr.cvtColor_BGR2LAB();
        img_LAB_FC = img_LAB.convertTo(21);
        print(img_LAB.type());
        print(img_LAB_FC.type());

        img_L = img_LAB_FC.extractChannel(0);
        img_A = img_LAB_FC.extractChannel(1);
        img_B = img_LAB_FC.extractChannel(2);

        print(img_L.type());
        print(img_A.type());
        print(img_B.type());

        print((double)(100.0 / 255.0));

        img_L_ori = img_L.multiply_scalar((double)(100.0 / 255.0));
        img_A_ori = img_A.subtract_scalar((double)128.0);
        img_B_ori = img_B.subtract_scalar((double)128.0);

        img_L_ori.minMax(ref min, ref max);
        print("****");
        print("Min: " + min + "; Max: " + max);

        img_A_ori.minMax(ref min, ref max);
        print("****");
        print("Min: " + min + "; Max: " + max);

        img_B_ori.minMax(ref min, ref max);
        print("****");
        print("Min: " + min + "; Max: " + max);

        print(img_L_ori.type());
        print(img_A_ori.type());
        print(img_B_ori.type());

        double[,] cost = new double[numNeighbour, 3];

        int cnt = 0;

        //horintal neighbour
        for (int h = 0; h < height_test; h++)
        {
            for (int w = 0; w < width_test - 1; w++)
            {
                float L1 = (float)img_L_ori.get(h, w, 0, 5, 1);
                float L2 = (float)img_L_ori.get(h, w + 1, 0, 5, 1);
                float A1 = (float)img_A_ori.get(h, w, 0, 5, 1);
                float A2 = (float)img_A_ori.get(h, w + 1, 0, 5, 1);
                float B1 = (float)img_B_ori.get(h, w, 0, 5, 1);
                float B2 = (float)img_B_ori.get(h, w + 1, 0, 5, 1);
                float tmpDist = (L1 - L2) * (L1 - L2) + (A1 - A2) * (A1 - A2) + (B1 - B2) * (B1 - B2);
                cost[cnt, 0] = w + h * width_test;
                cost[cnt, 1] = w + h * width_test + 1;
                cost[cnt,2] = scale * Mathf.Exp((-tmpDist) / (2 * sigma * sigma));
                cnt++;
            }
        }

        //vertical neighbour
        for (int h = 0; h < height_test - 1; h++)
        {
            for (int w = 0; w < width_test; w++)
            {
                float L1 = (float)img_L_ori.get(h, w, 0, 5, 1);
                float L2 = (float)img_L_ori.get(h + 1, w, 0, 5, 1);
                float A1 = (float)img_A_ori.get(h, w, 0, 5, 1);
                float A2 = (float)img_A_ori.get(h + 1, w, 0, 5, 1);
                float B1 = (float)img_B_ori.get(h, w, 0, 5, 1);
                float B2 = (float)img_B_ori.get(h + 1, w, 0, 5, 1);
                float tmpDist = (L1 - L2) * (L1 - L2) + (A1 - A2) * (A1 - A2) + (B1 - B2) * (B1 - B2);
                cost[cnt, 0] = w + h * width_test;
                cost[cnt, 1] = w + h * width_test + width_test;
                cost[cnt, 2] = scale * Mathf.Exp((-tmpDist) / (2 * sigma * sigma));
                cnt++;
            }
        }

        //diagonal neighbour
        for (int h = 0; h < height_test - 1; h++)
        {
            for (int w = 0; w < width_test - 1; w++)
            {
                float L1 = (float)img_L_ori.get(h, w, 0, 5, 1);
                float L2 = (float)img_L_ori.get(h + 1, w + 1, 0, 5, 1);
                float A1 = (float)img_A_ori.get(h, w, 0, 5, 1);
                float A2 = (float)img_A_ori.get(h + 1, w + 1, 0, 5, 1);
                float B1 = (float)img_B_ori.get(h, w, 0, 5, 1);
                float B2 = (float)img_B_ori.get(h + 1, w + 1, 0, 5, 1);
                float tmpDist = (L1 - L2) * (L1 - L2) + (A1 - A2) * (A1 - A2) + (B1 - B2) * (B1 - B2);
                cost[cnt, 0] = w + h * width_test;
                cost[cnt, 1] = w + h * width_test + width_test + 1;
                cost[cnt, 2] = scale * Mathf.Exp((-tmpDist) / (2 * sigma * sigma)) * (1.0 / Mathf.Sqrt((float)2.0));
                cnt++;
            }
        }

        print(numNeighbour);
        print(cnt);

        //set graph and perform graph cut algorithm
        GraphCutOptimizationGeneralGraph gCut = new GraphCutOptimizationGeneralGraph(numSite, numLabel);

        //set data cost
        gCut.setDataCost(unary_data);

        //set smooth cost
        gCut.setSmoothCost(smooth);

        //set neighbour cost
        for (int i = 0; i < numNeighbour; i++)
            gCut.setNeighbors((int)cost[i, 0], (int)cost[i, 1], (int)cost[i, 2]);

        print("Energy before optimization is: " + gCut.compute_energy());
        gCut.expansion(5);
        print("Energy after optimization is: " + gCut.compute_energy());

        cvMat rstGc = new cvMat(height_test, width_test, 0, 0);

        for (int h = 0; h < height_test; h++)
        {
            for (int w = 0; w < width_test; w++)
            {
                int val = (int)(255 * gCut.whatLabel(w + h * width_test));
                rstGc.set(h, w, 0, 0, 1, (double)val);
                //rst.at<float>(h, w) = gc->whatLabel(w + h * width);
            }
        }

        rstGc.show(5);

        rstGc.Dispose();

        gCut.Dispose();

        img_LAB.Dispose();
        img_LAB_FC.Dispose();
        img_L.Dispose();
        img_A.Dispose();
        img_B.Dispose();
        img_L_ori.Dispose();
        img_A_ori.Dispose();
        img_B_ori.Dispose();
        output_DNN_prob_normalize.Dispose();

        im_bgr.Dispose();
        im_pos_click.Dispose();
        im_neg_click.Dispose();
        output_DNN_0.Dispose();
        output_DNN_1.Dispose();

        net.Dispose();
        
        print("End of the line");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
