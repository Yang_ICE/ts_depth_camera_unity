﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;
using System;

public class test_GCO : MonoBehaviour {

    // Use this for initialization
    void Start() {

        int width = 10;
        int height = 5;
        int num_pixels = width * height;
        int num_labels = 7;

        int[] result = new int[num_pixels];

        //data cost
        int[] data = new int[num_pixels * num_labels];        
        for (int i = 0; i < num_pixels; i=i+1)
            for (int l = 0; l < num_labels; l=l+1)
                if (i < 25)
                {
                    if (l == 0) data[i * num_labels + l] = 0;
                    else data[i * num_labels + l] = 10;
                }
                else
                {
                    if (l == 5) data[i * num_labels + l] = 0;
                    else data[i * num_labels + l] = 10;
                }

        //smooth cost
        int[] smooth = new int[num_labels * num_labels];
        for (int l1 = 0; l1 < num_labels; l1++)
            for (int l2 = 0; l2 < num_labels; l2++)
                smooth[l1 + l2 * num_labels] = (l1 - l2) * (l1 - l2) <= 4 ? (l1 - l2) * (l1 - l2) : 4;

        GraphCutOptimizationGeneralGraph gCut = new GraphCutOptimizationGeneralGraph(num_pixels, num_labels);

        //set data cost
        gCut.setDataCost(data);

        //set smooth cost
        gCut.setSmoothCost(smooth);

        // set neighbour and weight
        
        
        //general 4-neighbour system
        // first set up horizontal neighbors
        for (int y = 0; y < height; y++)
            for (int x = 1; x < width; x++)
                gCut.setNeighbors(x + y * width, x - 1 + y * width, 1);

        // next set up vertical neighbors
        for (int y = 1; y < height; y++)
            for (int x = 0; x < width; x++)
                gCut.setNeighbors(x + y * width, x + (y - 1) * width, 1);


        /*
        //another neightbour system in the test case with the toolbox
        // first set up horizontal neighbors
        for (int y = 0; y < height; y++)
            for (int x = 1; x < width; x++)
            {
                int p1 = x - 1 + y * width;
                int p2 = x + y * width;
                gCut.setNeighbors(p1, p2, p1 + p2);
            }

        // next set up vertical neighbors
        for (int y = 1; y < height; y++)
            for (int x = 0; x < width; x++)
            {
                int p1 = x + (y - 1) * width;
                int p2 = x + y * width;
                gCut.setNeighbors(p1, p2, p1 * p2);
            }
        */

        print("Energy before optimization is: " + gCut.compute_energy());
        gCut.expansion(2);
        print("Energy after optimization is: " + gCut.compute_energy());
        
        for (int i = 0; i < num_pixels; i++)
            result[i] = gCut.whatLabel(i);

        gCut.Dispose();
        print("End of line");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
