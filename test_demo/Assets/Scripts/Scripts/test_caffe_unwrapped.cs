﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;
using System;
using System.Text;

public class test_caffe_unwrapped : MonoBehaviour {

    [DllImport("caffe_net")]
    public static extern IntPtr CreateNet();

    [DllImport("caffe_net")]
    public static extern void DisposeNet(IntPtr net);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_mode(IntPtr net, int mode);

    [DllImport("caffe_net")]
    public static extern bool caffe_net_check_device(IntPtr net, int id);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_device(IntPtr net, int id);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_model(IntPtr net, StringBuilder model, int len);

    [DllImport("caffe_net")]
    public static extern void caffe_net_get_model(IntPtr net, StringBuilder model);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_weight(IntPtr net, StringBuilder model, int len);

    [DllImport("caffe_net")]
    public static extern void caffe_net_get_weight_file(IntPtr net, StringBuilder model);

    [DllImport("caffe_net")]
    public static extern void caffe_net_get_input_size(IntPtr net, ref int height, ref int width, ref int channel, ref int number);

    [DllImport("caffe_net")]
    public static extern void caffe_net_input_reshape(IntPtr net, int height, int width, int channel, int number);

    [DllImport("caffe_net")]
    public static extern void caffe_net_bridge_mutable_input_data(IntPtr net);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_input_data(IntPtr net, int idx, double val);

    [DllImport("caffe_net")]
    public static extern double caffe_net_get_input_data(IntPtr net, int idx);

    [DllImport("caffe_net")]
    public static extern void caffe_net_bridge_output_data(IntPtr net);

    [DllImport("caffe_net")]
    public static extern double caffe_net_get_output_data(IntPtr net, int idx);

    [DllImport("caffe_net")]
    public static extern void caffe_net_forward_pass(IntPtr net);

    // Use this for initialization
    void Start () {

        IntPtr net = CreateNet();

        print("******************************");
        print("test mode and device");

        caffe_net_set_mode(net, 0);

        if (caffe_net_check_device(net, 0))
            print("Device 0 exists");
        else
            print("Device 0 does not exist");

        if (caffe_net_check_device(net, 1))
            print("Device 1 exists");
        else
            print("Device 1 does not exist");

        caffe_net_set_mode(net, 1);

        if (caffe_net_check_device(net, 0))
            print("Device 0 exists");
        else
            print("Device 0 does not exist");

        if (caffe_net_check_device(net, 1))
            print("Device 1 exists");
        else
            print("Device 1 does not exist");

        caffe_net_set_mode(net, 1);
        caffe_net_set_device(net, 0);

        //caffe_net_set_device(net, 1);

        print("******************************");
        print("test model and weight");

        StringBuilder model = new StringBuilder(1024);
        model.Append("C:/Code/fcn_berkeleyvision_org/voc-fcnClickPattern8s/deploy.prototxt");

        caffe_net_set_model(net, model, model.Length);

        StringBuilder model_out = new StringBuilder(1024);
        caffe_net_get_model(net, model_out);
        print(model_out.ToString());

        StringBuilder weight_file = new StringBuilder(1024);
        weight_file.Append("C:/Code/fcn_berkeleyvision_org/voc-fcnClickPattern8s/snapshot/train_iter_500000.caffemodel");
        caffe_net_set_weight(net, weight_file, weight_file.Length);

        StringBuilder weight_file_out = new StringBuilder(1024);
        caffe_net_get_weight_file(net, weight_file_out);
        print(weight_file_out.ToString());

        print("******************************");
        print("test set input");

        int height = 0;
        int width = 0;
        int channel = 0;
        int number = 0;

        int height_test = 281;
        int width_test = 500;

        caffe_net_get_input_size(net, ref height, ref width, ref channel, ref number);

        print("Size of input before reshape: " + height + ", " + width + ", " + channel + ", " + number);

        caffe_net_input_reshape(net, height_test, width_test, channel, number);

        caffe_net_get_input_size(net, ref height, ref width, ref channel, ref number);

        print("Size of input after reshape: " + height + ", " + width + ", " + channel + ", " + number);

        caffe_net_bridge_mutable_input_data(net);

        //print(caffe_net_get_input_data(net, 100));

        //caffe_net_set_input_data(net, 100, (double)70.5);

        //print(caffe_net_get_input_data(net, 100));

        StringBuilder file = new StringBuilder(500);
        file.Append("E:/Dataset/VOC2012/VOC2012/myImage/2007_000032.tiff");
        cvMat im_bgr = new cvMat(file);
        im_bgr.show(0);
        file.Remove(0, file.Length);

        file.Append("E:/Dataset/VOC2012/VOC2012/myImage/clickPattern/2007_000032_3_posClk_s1_1.png");
        cvMat im_pos_click = new cvMat(file);
        im_pos_click.show(1);
        file.Remove(0, file.Length);

        file.Append("E:/Dataset/VOC2012/VOC2012/myImage/clickPattern/2007_000032_3_negClk_s1_1.png");
        cvMat im_neg_click = new cvMat(file);
        im_neg_click.show(2);
        file.Remove(0, file.Length);

        
        for (int h = 0; h < height_test; h++)
        {
            for (int w = 0; w < width_test; w++)
            {
                double val = 0;

                val = im_bgr.get(h, w, 0, 16, 3) - (double)104.00698793;
                caffe_net_set_input_data(net, w + h * width_test, val);

                val = im_bgr.get(h, w, 1, 16, 3) - (double)116.66876762;
                caffe_net_set_input_data(net, w + h * width_test + height_test * width_test, val);

                val = im_bgr.get(h, w, 2, 16, 3) - (double)122.67891434;
                caffe_net_set_input_data(net, w + h * width_test + 2 * height_test * width_test, val);

                val = im_pos_click.get(h, w, 0, 0, 1) - (double)166.90557;
                caffe_net_set_input_data(net, w + h * width_test + 3 * height_test * width_test, val);

                val = im_neg_click.get(h, w, 0, 0, 1) - (double)147.47697;
                caffe_net_set_input_data(net, w + h * width_test + 4 * height_test * width_test, val);
                //inputData[w + h * width_test] = img_BGR[0].at<float>(h, w);
                //inputData[w + h * width_test + height_test * width_test] = img_BGR[1].at<float>(h, w);
                //inputData[w + h * width_test + 2 * height_test * width_test] = img_BGR[2].at<float>(h, w);
                //inputData[w + h * width_test + 3 * height_test * width_test] = imgPosClick.at<float>(h, w);
                //inputData[w + h * width_test + 4 * height_test * width_test] = imgNegClick.at<float>(h, w);
            }
        }
        
        caffe_net_forward_pass(net);

        caffe_net_bridge_output_data(net);

       // print(caffe_net_get_output_data(net, 10));
        
        cvMat output_DNN_0 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1 = new cvMat(height_test, width_test, 5, 0);

        for (int h = 0; h < height_test; h++)
        {
            for (int w = 0; w < width_test; w++)
            {
                double val = 0;

                val = caffe_net_get_output_data(net, w + h * width_test);
                output_DNN_0.set(h, w, 0, 5, 1, val);

                val = caffe_net_get_output_data(net, w + h * width_test + height_test * width_test);
                output_DNN_1.set(h, w, 0, 5, 1, val);

                //output_DNN_0.at<float>(h, w) = output_data[w + h * width];
                //output_DNN_1.at<float>(h, w) = output_data[w + h * width + height * width];
            }
        }

        double min = 0;
        double max = 0;

        cvMat tmp = new cvMat();
        cvMat vis = new cvMat();
        output_DNN_1.minMax(ref min, ref max);

        tmp = output_DNN_1.normalize(min, max, (double)255);

        vis = tmp.convertTo(0);
        vis.show(3);
        vis.Dispose();

        tmp.Dispose();

        cvMat output_DNN_0_neg = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1_neg = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_0_exp = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1_exp = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_0_exp_p1 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1_exp_p1 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_0_exp_p1_d1 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_1_exp_p1_d1 = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_sum = new cvMat(height_test, width_test, 5, 0);
        cvMat output_DNN_prob = new cvMat(height_test, width_test, 5, 0);

        output_DNN_0_neg = output_DNN_0.multiply_scalar((double)(-1.0));
        output_DNN_1_neg = output_DNN_1.multiply_scalar((double)(-1.0));

        output_DNN_0_exp = output_DNN_0_neg.exp();
        output_DNN_1_exp = output_DNN_1_neg.exp();

        output_DNN_0_exp_p1 = output_DNN_0_exp.add_scalar((double)1.0);
        output_DNN_1_exp_p1 = output_DNN_1_exp.add_scalar((double)1.0);

        output_DNN_0_exp_p1_d1 = output_DNN_0_exp_p1.divide_scalar((double)1.0);
        output_DNN_1_exp_p1_d1 = output_DNN_1_exp_p1.divide_scalar((double)1.0);

        output_DNN_sum = output_DNN_0_exp_p1_d1.add(output_DNN_1_exp_p1_d1);

        output_DNN_prob = output_DNN_1_exp_p1_d1.divide(output_DNN_sum);

        output_DNN_prob.minMax(ref min, ref max);

        tmp = output_DNN_prob.normalize(min, max, (double)255);

        vis = tmp.convertTo(0);
        vis.show(4);
        vis.Dispose();

        tmp.Dispose();

        output_DNN_0_neg.Dispose();
        output_DNN_1_neg.Dispose();
        output_DNN_0_exp.Dispose();
        output_DNN_1_exp.Dispose();
        output_DNN_0_exp_p1.Dispose();
        output_DNN_1_exp_p1.Dispose();
        output_DNN_0_exp_p1_d1.Dispose();
        output_DNN_1_exp_p1_d1.Dispose();
        output_DNN_sum.Dispose();
        output_DNN_prob.Dispose();

        im_bgr.Dispose();
        im_pos_click.Dispose();
        im_neg_click.Dispose();
        output_DNN_0.Dispose();
        output_DNN_1.Dispose();

        DisposeNet(net);
        print("End of the line");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
