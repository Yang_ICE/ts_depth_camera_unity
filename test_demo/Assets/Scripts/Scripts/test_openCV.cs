﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;
using System;
using System.Text;

/*
CV_8UC1 0
CV_8UC3 16
CV_32FC1 5
CV_32FC3 21
CV_64FC1 6
CV_64FC3 22
*/

public class test_openCV : MonoBehaviour {

    // Use this for initialization
    void Start () {
        print("*********************");
        print("test Mat create and basic functions");
        cvMat mat1 = new cvMat();
        print(mat1.type());
        mat1.Dispose();
        print("****");

        print("test zero");
        mat1 = new cvMat(480, 640, 0, 0);
        print(mat1.type());
        print(mat1.get(100, 200, 0, 0, 1));
        mat1.set(100, 200, 0, 0, 1, (double)50);
        print(mat1.get(100, 200, 0, 0, 1));
        //cvMat mat2 = new cvMat(mat1);
        cvMat mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 0, 0, 1));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 16, 0);
        print(mat1.type());
        print(mat1.get(100, 200, 2, 16, 3));
        mat1.set(100, 200, 2, 16, 3, (double)60);
        print(mat1.get(100, 200, 2, 16, 3));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 2, 16, 3));        
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 5, 0);
        print(mat1.type());
        print(mat1.get(100, 200, 0, 5, 1));
        mat1.set(100, 200, 0, 5, 1, (double)70);
        print(mat1.get(100, 200, 0, 5, 1));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 0, 5, 1));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 21, 0);
        print(mat1.type());
        print(mat1.get(100, 200, 1, 21, 3));
        mat1.set(100, 200, 1, 21, 3, (double)20);
        print(mat1.get(100, 200, 1, 21, 3));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 1, 21, 3));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 6, 0);
        print(mat1.type());
        print(mat1.get(100, 200, 0, 6, 1));
        mat1.set(100, 200, 0, 6, 1, (double)30);
        print(mat1.get(100, 200, 0, 6, 1));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 0, 6, 1));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 22, 0);
        print(mat1.type());
        print(mat1.get(100, 200, 2, 22, 3));
        mat1.set(100, 200, 2, 22, 3, (double)40);
        print(mat1.get(100, 200, 2, 22, 3));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 2, 22, 3));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        print("test ones");
        mat1 = new cvMat(480, 640, 0, 1);
        print(mat1.type());
        print(mat1.get(100, 200, 0, 0, 1));
        mat1.set(100, 200, 0, 0, 1, (double)50);
        print(mat1.get(100, 200, 0, 0, 1));
        //cvMat mat2 = new cvMat(mat1);
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 0, 0, 1));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 16, 1);
        print(mat1.type());
        print(mat1.get(100, 200, 0, 16, 3));
        mat1.set(100, 200, 0, 16, 3, (double)60);
        print(mat1.get(100, 200, 0, 16, 3));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 0, 16, 3));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 5, 1);
        print(mat1.type());
        print(mat1.get(100, 200, 0, 5, 1));
        mat1.set(100, 200, 0, 5, 1, (double)70);
        print(mat1.get(100, 200, 0, 5, 1));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 0, 5, 1));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 21, 1);
        print(mat1.type());
        print(mat1.get(100, 200, 0, 21, 3));
        mat1.set(100, 200, 0, 21, 3, (double)20);
        print(mat1.get(100, 200, 0, 21, 3));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 0, 21, 3));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 6, 1);
        print(mat1.type());
        print(mat1.get(100, 200, 0, 6, 1));
        mat1.set(100, 200, 0, 6, 1, (double)30);
        print(mat1.get(100, 200, 0, 6, 1));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 0, 6, 1));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        mat1 = new cvMat(480, 640, 22, 1);
        print(mat1.type());
        print(mat1.get(100, 200, 0, 22, 3));
        mat1.set(100, 200, 0, 22, 3, (double)40);
        print(mat1.get(100, 200, 0, 22, 3));
        mat2 = mat1.copy();
        print(mat2.type());
        print(mat2.get(100, 200, 0, 22, 3));
        mat1.Dispose();
        mat2.Dispose();
        print("****");

        print("test binary comparison and show");
        mat1 = new cvMat(480, 640, 0, 0);

        for (int h = 10; h < 150; h++)
        {
            for (int w = 20; w < 200; w++)
            {
                mat1.set(h, w, 0, 0, 1, (double)128);
            }
        }

        for (int h = 170; h < 320; h++)
        {
            for (int w = 260; w < 310; w++)
            {
                mat1.set(h, w, 0, 0, 1, (double)210);
            }
        }

        mat1.show(0);

        mat2 = mat1.isEqual((double)128);
        mat2.show(1);
        mat2.Dispose();

        mat2 = mat1.isNotEqual((double)210);
        mat2.show(2);
        mat2.Dispose();

        mat2 = mat1.isLess((double)150);
        mat2.show(3);
        mat2.Dispose();

        mat2 = mat1.isGreater((double)170);
        mat2.show(4);

        mat1.setTo_mask(60, mat2);

        mat1.show(5);

        mat2 = mat1.copy();
        mat2.show(6);

        mat2.Dispose();
        mat1.Dispose();

        print("*********************");
        print("test type convert");

        int[] typeSet_C1 = { 0, 5, 6 };
        int[] typeSet_C3 = { 16, 21, 22 };

        for (int i = 0; i < 3; i = i + 1)
            for (int j = 0; j < 3; j = j + 1)
            {
                mat1 = new cvMat(480, 640, typeSet_C1[i], 0);
                print("Source type: " + mat1.type());
                mat2 = mat1.convertTo(typeSet_C1[j]);
                print("Dst type after convert: " + mat2.type());
                print("Dst type after convert should be: " + typeSet_C1[j]);
                mat1.Dispose();
                mat2.Dispose();
                print("***");
            }

        for (int i = 0; i < 3; i = i + 1)
            for (int j = 0; j < 3; j = j + 1)
            {
                mat1 = new cvMat(480, 640, typeSet_C3[i], 0);
                print("Source type: " + mat1.type());
                mat2 = mat1.convertTo(typeSet_C3[j]);
                print("Dst type after convert: " + mat2.type());
                print("Dst type after convert should be: " + typeSet_C3[j]);
                mat1.Dispose();
                mat2.Dispose();
                print("***");
            }

        print("*********************");
        print("test minMax and normalization");

        cvMat matVis;
        mat1 = new cvMat(480, 640, 5, 1);

        for (int h = 100; h < 270; h++)
        {
            for (int w = 20; w < 200; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)2.0);
            }
        }

        for (int h = 300; h < 420; h++)
        {
            for (int w = 230; w < 400; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)3.0);
            }
        }

        matVis = mat1.convertTo(0);
        matVis.show(7);

        double minVal = 0;
        double maxVal = 0;

        mat1.minMax(ref minVal, ref maxVal);

        print("min: " + minVal + ", " + "max: " + maxVal);

        mat2 = mat1.normalize(minVal, maxVal, (double)255.0);

        cvMat mat_mask;
        mat_mask = mat1.isNotEqual((double)1);

        mat1.minMax_mask(ref minVal, ref maxVal, mat_mask);

        print("min with mask: " + minVal + ", " + "max with mask: " + maxVal);

        mat2.minMax(ref minVal, ref maxVal);

        print("min after normalization: " + minVal + ", " + "max after normalization: " + maxVal);

        mat2.minMax_mask(ref minVal, ref maxVal, mat_mask);

        print("min after normalization with mask: " + minVal + ", " + "max after normalization with mask: " + maxVal);

        matVis = mat2.convertTo(0);
        matVis.show(8);

        mat2.Dispose();
        matVis.Dispose();

        print("************************");
        print("test add weighted");

        cvMat mat_rst;

        mat1 = new cvMat(480, 640, 5, 0);

        for (int h = 20; h < 170; h++)
        {
            for (int w = 100; w < 300; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)60.0);
            }
        }

        for (int h = 200; h < 370; h++)
        {
            for (int w = 400; w < 600; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)60.0);
            }
        }

        matVis = mat1.convertTo(0);

        matVis.show(9);

        matVis.Dispose();

        mat2 = new cvMat(480, 640, 5, 0);

        for (int h = 100; h < 270; h++)
        {
            for (int w = 20; w < 200; w++)
            {
                mat2.set(h, w, 0, 5, 1, (double)120.0);
            }
        }

        for (int h = 300; h < 420; h++)
        {
            for (int w = 350; w < 500; w++)
            {
                mat2.set(h, w, 0, 5, 1, (double)170.0);
            }
        }

        matVis = mat2.convertTo(0);

        matVis.show(10);

        matVis.Dispose();

        mat_rst = mat1.addWeighted((double)0.5, mat2, (double)0.5, (double)0.0);

        matVis = mat_rst.convertTo(0);

        matVis.show(11);

        matVis.Dispose();

        print("************************");
        print("test subtract scalar");

        mat1 = new cvMat(480, 640, 5, 0);

        for (int h = 0; h < 480; h++)
        {
            for (int w = 0; w < 640; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)255.0);
            }
        }

        for (int h = 140; h < 340; h++)
        {
            for (int w = 120; w < 420; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)128.0);
            }
        }

        matVis = mat1.convertTo(0);
        matVis.show(12);

        mat_rst = mat1.subtract_scalar((double)60.0);

        matVis = mat_rst.convertTo(0);
        matVis.show(13);

        mat1.Dispose();
        mat_rst.Dispose();
        matVis.Dispose();

        print("************************");
        print("test multiply");

        mat1 = new cvMat(7, 7, 5, 0);
        mat2 = new cvMat(7, 7, 5, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)(h + w));
                mat2.set(h, w, 0, 5, 1, (double)(h + w + 1));
            }
        }

        mat_rst = mat1.multiply(mat2);

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 5, 1) + ", " + mat1.get(h, 1, 0, 5, 1) + ", " + mat1.get(h, 2, 0, 5, 1) + ", " + mat1.get(h, 3, 0, 5, 1) + ", " + mat1.get(h, 4, 0, 5, 1) + ", " + mat1.get(h, 5, 0, 5, 1) + ", " + mat1.get(h, 6, 0, 5, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat2.get(h, 0, 0, 5, 1) + ", " + mat2.get(h, 1, 0, 5, 1) + ", " + mat2.get(h, 2, 0, 5, 1) + ", " + mat2.get(h, 3, 0, 5, 1) + ", " + mat2.get(h, 4, 0, 5, 1) + ", " + mat2.get(h, 5, 0, 5, 1) + ", " + mat2.get(h, 6, 0, 5, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 5, 1) + ", " + mat_rst.get(h, 1, 0, 5, 1) + ", " + mat_rst.get(h, 2, 0, 5, 1) + ", " + mat_rst.get(h, 3, 0, 5, 1) + ", " + mat_rst.get(h, 4, 0, 5, 1) + ", " + mat_rst.get(h, 5, 0, 5, 1) + ", " + mat_rst.get(h, 6, 0, 5, 1));
        }

        mat2.Dispose();
        mat_rst.Dispose();

        print("****");

        mat_rst = mat1.multiply(mat1);

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 5, 1) + ", " + mat1.get(h, 1, 0, 5, 1) + ", " + mat1.get(h, 2, 0, 5, 1) + ", " + mat1.get(h, 3, 0, 5, 1) + ", " + mat1.get(h, 4, 0, 5, 1) + ", " + mat1.get(h, 5, 0, 5, 1) + ", " + mat1.get(h, 6, 0, 5, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 5, 1) + ", " + mat_rst.get(h, 1, 0, 5, 1) + ", " + mat_rst.get(h, 2, 0, 5, 1) + ", " + mat_rst.get(h, 3, 0, 5, 1) + ", " + mat_rst.get(h, 4, 0, 5, 1) + ", " + mat_rst.get(h, 5, 0, 5, 1) + ", " + mat_rst.get(h, 6, 0, 5, 1));
        }

        mat1.Dispose();
        mat_rst.Dispose();

        print("****");

        mat1 = new cvMat(7, 7, 5, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)(h + w));
            }
        }

        mat_rst = mat1.multiply_scalar((double)5);

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 5, 1) + ", " + mat1.get(h, 1, 0, 5, 1) + ", " + mat1.get(h, 2, 0, 5, 1) + ", " + mat1.get(h, 3, 0, 5, 1) + ", " + mat1.get(h, 4, 0, 5, 1) + ", " + mat1.get(h, 5, 0, 5, 1) + ", " + mat1.get(h, 6, 0, 5, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 5, 1) + ", " + mat_rst.get(h, 1, 0, 5, 1) + ", " + mat_rst.get(h, 2, 0, 5, 1) + ", " + mat_rst.get(h, 3, 0, 5, 1) + ", " + mat_rst.get(h, 4, 0, 5, 1) + ", " + mat_rst.get(h, 5, 0, 5, 1) + ", " + mat_rst.get(h, 6, 0, 5, 1));
        }

        mat1.Dispose();
        mat_rst.Dispose();

        print("************************");
        print("test Mat add");

        mat1 = new cvMat(7, 7, 5, 0);
        mat2 = new cvMat(7, 7, 5, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)(h + w));
                mat2.set(h, w, 0, 5, 1, (double)(h + w + 1));
            }
        }

        mat_rst = mat1.add(mat2);

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 5, 1) + ", " + mat1.get(h, 1, 0, 5, 1) + ", " + mat1.get(h, 2, 0, 5, 1) + ", " + mat1.get(h, 3, 0, 5, 1) + ", " + mat1.get(h, 4, 0, 5, 1) + ", " + mat1.get(h, 5, 0, 5, 1) + ", " + mat1.get(h, 6, 0, 5, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat2.get(h, 0, 0, 5, 1) + ", " + mat2.get(h, 1, 0, 5, 1) + ", " + mat2.get(h, 2, 0, 5, 1) + ", " + mat2.get(h, 3, 0, 5, 1) + ", " + mat2.get(h, 4, 0, 5, 1) + ", " + mat2.get(h, 5, 0, 5, 1) + ", " + mat2.get(h, 6, 0, 5, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 5, 1) + ", " + mat_rst.get(h, 1, 0, 5, 1) + ", " + mat_rst.get(h, 2, 0, 5, 1) + ", " + mat_rst.get(h, 3, 0, 5, 1) + ", " + mat_rst.get(h, 4, 0, 5, 1) + ", " + mat_rst.get(h, 5, 0, 5, 1) + ", " + mat_rst.get(h, 6, 0, 5, 1));
        }

        mat1.Dispose();
        mat2.Dispose();
        mat_rst.Dispose();

        print("****");

        mat1 = new cvMat(7, 7, 5, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)(h + w));
            }
        }

        mat_rst=mat1.add_scalar((double)2.0);

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 5, 1) + ", " + mat1.get(h, 1, 0, 5, 1) + ", " + mat1.get(h, 2, 0, 5, 1) + ", " + mat1.get(h, 3, 0, 5, 1) + ", " + mat1.get(h, 4, 0, 5, 1) + ", " + mat1.get(h, 5, 0, 5, 1) + ", " + mat1.get(h, 6, 0, 5, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 5, 1) + ", " + mat_rst.get(h, 1, 0, 5, 1) + ", " + mat_rst.get(h, 2, 0, 5, 1) + ", " + mat_rst.get(h, 3, 0, 5, 1) + ", " + mat_rst.get(h, 4, 0, 5, 1) + ", " + mat_rst.get(h, 5, 0, 5, 1) + ", " + mat_rst.get(h, 6, 0, 5, 1));
        }

        mat1.Dispose();
        mat_rst.Dispose();

        print("************************");
        print("test Mat divide");

        double numerator = 2.0;
        mat1 = new cvMat(7, 7, 5, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 5, 1, (double)(h + w + 1));
            }
        }

        mat_rst = mat1.divide_scalar(numerator);

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 5, 1) + ", " + mat1.get(h, 1, 0, 5, 1) + ", " + mat1.get(h, 2, 0, 5, 1) + ", " + mat1.get(h, 3, 0, 5, 1) + ", " + mat1.get(h, 4, 0, 5, 1) + ", " + mat1.get(h, 5, 0, 5, 1) + ", " + mat1.get(h, 6, 0, 5, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 5, 1) + ", " + mat_rst.get(h, 1, 0, 5, 1) + ", " + mat_rst.get(h, 2, 0, 5, 1) + ", " + mat_rst.get(h, 3, 0, 5, 1) + ", " + mat_rst.get(h, 4, 0, 5, 1) + ", " + mat_rst.get(h, 5, 0, 5, 1) + ", " + mat_rst.get(h, 6, 0, 5, 1));
        }

        mat1.Dispose();
        mat_rst.Dispose();

        print("****");

        mat1 = new cvMat(7, 7, 6, 0);
        mat2 = new cvMat(7, 7, 6, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 6, 1, (double)(h + w + 1));
                mat2.set(h, w, 0, 6, 1, (double)(h + w + 2));
            }
        }

        mat_rst = mat1.divide(mat2);

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1) + ", " + mat1.get(h, 5, 0, 6, 1) + ", " + mat1.get(h, 6, 0, 6, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat2.get(h, 0, 0, 6, 1) + ", " + mat2.get(h, 1, 0, 6, 1) + ", " + mat2.get(h, 2, 0, 6, 1) + ", " + mat2.get(h, 3, 0, 6, 1) + ", " + mat2.get(h, 4, 0, 6, 1) + ", " + mat2.get(h, 5, 0, 6, 1) + ", " + mat2.get(h, 6, 0, 6, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 6, 1) + ", " + mat_rst.get(h, 1, 0, 6, 1) + ", " + mat_rst.get(h, 2, 0, 6, 1) + ", " + mat_rst.get(h, 3, 0, 6, 1) + ", " + mat_rst.get(h, 4, 0, 6, 1) + ", " + mat_rst.get(h, 5, 0, 6, 1) + ", " + mat_rst.get(h, 6, 0, 6, 1));
        }

        mat1.Dispose();
        mat2.Dispose();
        mat_rst.Dispose();

        print("************************");
        print("test Mat sqrt");

        mat1 = new cvMat(7, 7, 6, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 6, 1, (double)(h + w));
            }
        }

        mat_rst = mat1.sqrt();

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1) + ", " + mat1.get(h, 5, 0, 6, 1) + ", " + mat1.get(h, 6, 0, 6, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 6, 1) + ", " + mat_rst.get(h, 1, 0, 6, 1) + ", " + mat_rst.get(h, 2, 0, 6, 1) + ", " + mat_rst.get(h, 3, 0, 6, 1) + ", " + mat_rst.get(h, 4, 0, 6, 1) + ", " + mat_rst.get(h, 5, 0, 6, 1) + ", " + mat_rst.get(h, 6, 0, 6, 1));
        }

        mat1.Dispose();
        mat_rst.Dispose();

        print("************************");
        print("test Mat min");

        mat1 = new cvMat(7, 7, 6, 0);
        mat2 = new cvMat(7, 7, 6, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 6, 1, (double)(h * h + w));
                mat2.set(h, w, 0, 6, 1, (double)(h + w * w));
            }
        }

        mat_rst = mat1.min(mat2);

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1) + ", " + mat1.get(h, 5, 0, 6, 1) + ", " + mat1.get(h, 6, 0, 6, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat2.get(h, 0, 0, 6, 1) + ", " + mat2.get(h, 1, 0, 6, 1) + ", " + mat2.get(h, 2, 0, 6, 1) + ", " + mat2.get(h, 3, 0, 6, 1) + ", " + mat2.get(h, 4, 0, 6, 1) + ", " + mat2.get(h, 5, 0, 6, 1) + ", " + mat2.get(h, 6, 0, 6, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 6, 1) + ", " + mat_rst.get(h, 1, 0, 6, 1) + ", " + mat_rst.get(h, 2, 0, 6, 1) + ", " + mat_rst.get(h, 3, 0, 6, 1) + ", " + mat_rst.get(h, 4, 0, 6, 1) + ", " + mat_rst.get(h, 5, 0, 6, 1) + ", " + mat_rst.get(h, 6, 0, 6, 1));
        }

        print("************************");
        print("test Mat exp");

        mat1 = new cvMat(7, 7, 6, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 6, 1, (double)(h + w));
            }
        }

        mat_rst = mat1.exp();

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1) + ", " + mat1.get(h, 5, 0, 6, 1) + ", " + mat1.get(h, 6, 0, 6, 1));
        }

        print("****");
        for (int h = 0; h < 7; h++)
        {
            print(mat_rst.get(h, 0, 0, 6, 1) + ", " + mat_rst.get(h, 1, 0, 6, 1) + ", " + mat_rst.get(h, 2, 0, 6, 1) + ", " + mat_rst.get(h, 3, 0, 6, 1) + ", " + mat_rst.get(h, 4, 0, 6, 1) + ", " + mat_rst.get(h, 5, 0, 6, 1) + ", " + mat_rst.get(h, 6, 0, 6, 1));
        }

        print("************************");
        print("test Mat bitwise and");

        mat1 = new cvMat(480, 640, 0, 0);
        mat2 = new cvMat(480, 640, 0, 0);

        for (int h = 100; h < 250; h++)
        {
            for (int w = 120; w < 360; w++)
            {
                mat1.set(h, w, 0, 0, 1, (double)1);
            }
        }

        for (int h = 170; h < 360; h++)
        {
            for (int w = 400; w < 600; w++)
            {
                mat1.set(h, w, 0, 0, 1, (double)1);
            }
        }

        matVis = mat1.normalize((double)0, (double)1, (double)255);

        matVis.show(14);

        for (int h = 150; h < 250; h++)
        {
            for (int w = 200; w < 500; w++)
            {
                mat2.set(h, w, 0, 0, 1, (double)1);
            }
        }

        matVis = mat2.normalize((double)0, (double)1, (double)255);

        matVis.show(15);

        mat_rst = mat1.bitwise_and(mat2);

        matVis = mat_rst.normalize((double)0, (double)1, (double)255);

        matVis.show(16);

        mat1.Dispose();
        mat2.Dispose();
        mat_rst.Dispose();
        matVis.Dispose();

        print("************************");
        print("test split");

        mat1 = new cvMat(480, 640, 16, 0);

        for (int h = 100; h < 250; h++)
        {
            for (int w = 120; w < 360; w++)
            {
                mat1.set(h, w, 0, 16, 3, (double)120);
            }
        }

        for (int h = 150; h < 350; h++)
        {
            for (int w = 270; w < 480; w++)
            {
                mat1.set(h, w, 1, 16, 3, (double)175);
            }
        }

        for (int h = 200; h < 420; h++)
        {
            for (int w = 320; w < 620; w++)
            {
                mat1.set(h, w, 2, 16, 3, (double)220);
            }
        }

        mat1.show(17);

        mat2 = mat1.extractChannel(0);

        mat2.show(18);

        mat2 = mat1.extractChannel(1);

        mat2.show(19);

        mat2 = mat1.extractChannel(2);

        mat2.show(20);

        print("************************");
        print("test meshgrid");

        int x_l = 0;
        int x_h = 7;
        int y_l = 0;
        int y_h = 4;

        cvMeshGrid mesh = new cvMeshGrid(x_l, x_h, y_l, y_h);

        print("****");
        print("get X and Y mesh individually");

        mat1 = mesh.get_meshX();
        for (int h = 0; h < 5; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1) + ", " + mat1.get(h, 5, 0, 6, 1) + ", " + mat1.get(h, 6, 0, 6, 1));
        }
        mat1.Dispose();

        print("****");

        mat1 = mesh.get_meshY();
        for (int h = 0; h < 5; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1) + ", " + mat1.get(h, 5, 0, 6, 1) + ", " + mat1.get(h, 6, 0, 6, 1));
        }
        mat1.Dispose();

        print("****");

        print("get X and Y mesh at the same time");

        mesh.get_mesh(ref mat1, ref mat2);

        for (int h = 0; h < 5; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1) + ", " + mat1.get(h, 5, 0, 6, 1) + ", " + mat1.get(h, 6, 0, 6, 1));
        }

        print("****");

        for (int h = 0; h < 5; h++)
        {
            print(mat2.get(h, 0, 0, 6, 1) + ", " + mat2.get(h, 1, 0, 6, 1) + ", " + mat2.get(h, 2, 0, 6, 1) + ", " + mat2.get(h, 3, 0, 6, 1) + ", " + mat2.get(h, 4, 0, 6, 1) + ", " + mat2.get(h, 5, 0, 6, 1) + ", " + mat2.get(h, 6, 0, 6, 1));
        }

        mat1.Dispose();
        mat2.Dispose();

        print("****");
        int range_l = 0;
        int range_h = 0;

        mesh.get_rangeX(ref range_l, ref range_h);
        print("Range X:" + range_l + " to " + range_h);

        print("****");

        mesh.get_rangeY(ref range_l, ref range_h);
        print("Range Y:" + range_l + " to " + range_h);

        print("****");

        mesh.set_rangeX(0, 5);
        mesh.get_rangeX(ref range_l, ref range_h);
        print("Range X:" + range_l + " to " + range_h);

        print("****");

        mesh.set_rangeY(0, 10);
        mesh.get_rangeY(ref range_l, ref range_h);
        print("Range Y:" + range_l + " to " + range_h);

        print("****");
        print("get X and Y mesh individually");

        mat1 = mesh.get_meshX();
        for (int h = 0; h < 10; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1));
        }
        mat1.Dispose();

        print("****");

        mat1 = mesh.get_meshY();
        for (int h = 0; h < 10; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1));
        }
        mat1.Dispose();

        print("****");
        print("get X and Y mesh at the same time");

        mesh.get_mesh(ref mat1, ref mat2);

        for (int h = 0; h < 10; h++)
        {
            print(mat1.get(h, 0, 0, 6, 1) + ", " + mat1.get(h, 1, 0, 6, 1) + ", " + mat1.get(h, 2, 0, 6, 1) + ", " + mat1.get(h, 3, 0, 6, 1) + ", " + mat1.get(h, 4, 0, 6, 1));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat2.get(h, 0, 0, 6, 1) + ", " + mat2.get(h, 1, 0, 6, 1) + ", " + mat2.get(h, 2, 0, 6, 1) + ", " + mat2.get(h, 3, 0, 6, 1) + ", " + mat2.get(h, 4, 0, 6, 1));
        }

        mat1.Dispose();
        mat2.Dispose();

        print("************************");
        print("test BGR2LAB");

        mat1 = new cvMat(10, 10, 16, 0);

        for (int h = 0; h < 7; h++)
        {
            for (int w = 0; w < 7; w++)
            {
                mat1.set(h, w, 0, 16, 3, (double)255);
            }
        }

        for (int h = 3; h < 10; h++)
        {
            for (int w = 3; w < 10; w++)
            {
                mat1.set(h, w, 1, 16, 3, (double)175);
            }
        }

        for (int h = 2; h < 9; h++)
        {
            for (int w = 2; w < 9; w++)
            {
                mat1.set(h, w, 2, 16, 3, (double)220);
            }
        }

        mat2 = mat1.cvtColor_BGR2LAB();

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat1.get(h, 0, 0, 16, 3) + ", " + mat1.get(h, 1, 0, 16, 3) + ", " + mat1.get(h, 2, 0, 16, 3) + ", " + mat1.get(h, 3, 0, 16, 3) + ", " + mat1.get(h, 4, 0, 16, 3) + ", " + mat1.get(h, 5, 0, 16, 3) + ", " + mat1.get(h, 6, 0, 16, 3) + ", " + mat1.get(h, 7, 0, 16, 3) + ", " + mat1.get(h, 8, 0, 16, 3) + ", " + mat1.get(h, 9, 0, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat2.get(h, 0, 0, 16, 3) + ", " + mat2.get(h, 1, 0, 16, 3) + ", " + mat2.get(h, 2, 0, 16, 3) + ", " + mat2.get(h, 3, 0, 16, 3) + ", " + mat2.get(h, 4, 0, 16, 3) + ", " + mat2.get(h, 5, 0, 16, 3) + ", " + mat2.get(h, 6, 0, 16, 3) + ", " + mat2.get(h, 7, 0, 16, 3) + ", " + mat2.get(h, 8, 0, 16, 3) + ", " + mat2.get(h, 9, 0, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat1.get(h, 0, 1, 16, 3) + ", " + mat1.get(h, 1, 1, 16, 3) + ", " + mat1.get(h, 2, 1, 16, 3) + ", " + mat1.get(h, 3, 1, 16, 3) + ", " + mat1.get(h, 4, 1, 16, 3) + ", " + mat1.get(h, 5, 1, 16, 3) + ", " + mat1.get(h, 6, 1, 16, 3) + ", " + mat1.get(h, 7, 1, 16, 3) + ", " + mat1.get(h, 8, 1, 16, 3) + ", " + mat1.get(h, 9, 1, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat2.get(h, 0, 1, 16, 3) + ", " + mat2.get(h, 1, 1, 16, 3) + ", " + mat2.get(h, 2, 1, 16, 3) + ", " + mat2.get(h, 3, 1, 16, 3) + ", " + mat2.get(h, 4, 1, 16, 3) + ", " + mat2.get(h, 5, 1, 16, 3) + ", " + mat2.get(h, 6, 1, 16, 3) + ", " + mat2.get(h, 7, 1, 16, 3) + ", " + mat2.get(h, 8, 1, 16, 3) + ", " + mat2.get(h, 9, 1, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat1.get(h, 0, 2, 16, 3) + ", " + mat1.get(h, 1, 2, 16, 3) + ", " + mat1.get(h, 2, 2, 16, 3) + ", " + mat1.get(h, 3, 2, 16, 3) + ", " + mat1.get(h, 4, 2, 16, 3) + ", " + mat1.get(h, 5, 2, 16, 3) + ", " + mat1.get(h, 6, 2, 16, 3) + ", " + mat1.get(h, 7, 2, 16, 3) + ", " + mat1.get(h, 8, 2, 16, 3) + ", " + mat1.get(h, 9, 2, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat2.get(h, 0, 2, 16, 3) + ", " + mat2.get(h, 1, 2, 16, 3) + ", " + mat2.get(h, 2, 2, 16, 3) + ", " + mat2.get(h, 3, 2, 16, 3) + ", " + mat2.get(h, 4, 2, 16, 3) + ", " + mat2.get(h, 5, 2, 16, 3) + ", " + mat2.get(h, 6, 2, 16, 3) + ", " + mat2.get(h, 7, 2, 16, 3) + ", " + mat2.get(h, 8, 2, 16, 3) + ", " + mat2.get(h, 9, 2, 16, 3));
        }

        mat2.Dispose();

        print("************************");
        print("test RGB2LAB");

        mat2 = mat1.cvtColor_RGB2LAB();

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat1.get(h, 0, 0, 16, 3) + ", " + mat1.get(h, 1, 0, 16, 3) + ", " + mat1.get(h, 2, 0, 16, 3) + ", " + mat1.get(h, 3, 0, 16, 3) + ", " + mat1.get(h, 4, 0, 16, 3) + ", " + mat1.get(h, 5, 0, 16, 3) + ", " + mat1.get(h, 6, 0, 16, 3) + ", " + mat1.get(h, 7, 0, 16, 3) + ", " + mat1.get(h, 8, 0, 16, 3) + ", " + mat1.get(h, 9, 0, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat2.get(h, 0, 0, 16, 3) + ", " + mat2.get(h, 1, 0, 16, 3) + ", " + mat2.get(h, 2, 0, 16, 3) + ", " + mat2.get(h, 3, 0, 16, 3) + ", " + mat2.get(h, 4, 0, 16, 3) + ", " + mat2.get(h, 5, 0, 16, 3) + ", " + mat2.get(h, 6, 0, 16, 3) + ", " + mat2.get(h, 7, 0, 16, 3) + ", " + mat2.get(h, 8, 0, 16, 3) + ", " + mat2.get(h, 9, 0, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat1.get(h, 0, 1, 16, 3) + ", " + mat1.get(h, 1, 1, 16, 3) + ", " + mat1.get(h, 2, 1, 16, 3) + ", " + mat1.get(h, 3, 1, 16, 3) + ", " + mat1.get(h, 4, 1, 16, 3) + ", " + mat1.get(h, 5, 1, 16, 3) + ", " + mat1.get(h, 6, 1, 16, 3) + ", " + mat1.get(h, 7, 1, 16, 3) + ", " + mat1.get(h, 8, 1, 16, 3) + ", " + mat1.get(h, 9, 1, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat2.get(h, 0, 1, 16, 3) + ", " + mat2.get(h, 1, 1, 16, 3) + ", " + mat2.get(h, 2, 1, 16, 3) + ", " + mat2.get(h, 3, 1, 16, 3) + ", " + mat2.get(h, 4, 1, 16, 3) + ", " + mat2.get(h, 5, 1, 16, 3) + ", " + mat2.get(h, 6, 1, 16, 3) + ", " + mat2.get(h, 7, 1, 16, 3) + ", " + mat2.get(h, 8, 1, 16, 3) + ", " + mat2.get(h, 9, 1, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat1.get(h, 0, 2, 16, 3) + ", " + mat1.get(h, 1, 2, 16, 3) + ", " + mat1.get(h, 2, 2, 16, 3) + ", " + mat1.get(h, 3, 2, 16, 3) + ", " + mat1.get(h, 4, 2, 16, 3) + ", " + mat1.get(h, 5, 2, 16, 3) + ", " + mat1.get(h, 6, 2, 16, 3) + ", " + mat1.get(h, 7, 2, 16, 3) + ", " + mat1.get(h, 8, 2, 16, 3) + ", " + mat1.get(h, 9, 2, 16, 3));
        }

        print("****");

        for (int h = 0; h < 10; h++)
        {
            print(mat2.get(h, 0, 2, 16, 3) + ", " + mat2.get(h, 1, 2, 16, 3) + ", " + mat2.get(h, 2, 2, 16, 3) + ", " + mat2.get(h, 3, 2, 16, 3) + ", " + mat2.get(h, 4, 2, 16, 3) + ", " + mat2.get(h, 5, 2, 16, 3) + ", " + mat2.get(h, 6, 2, 16, 3) + ", " + mat2.get(h, 7, 2, 16, 3) + ", " + mat2.get(h, 8, 2, 16, 3) + ", " + mat2.get(h, 9, 2, 16, 3));
        }

        mat2.Dispose();

        mat1.Dispose();

        print("************************");
        print("test initialize Mat with image");

        StringBuilder file = new StringBuilder(500);

        file.Append("E:/Dataset/VOC2012/VOC2012/myImage/2007_000032.tiff");
        mat1 = new cvMat(file);
        mat1.show(21);
        print(mat1.type());
        mat1.Dispose();
        file.Remove(0, file.Length);
        

        print("****");

        file.Append("E:/Dataset/VOC2012/VOC2012/myImage/clickPattern/2007_000032_3_posClk_s1_1.png");
        mat1 = new cvMat(file);
        mat1.show(22);
        print(mat1.type());
        mat1.Dispose();
        file.Remove(0, file.Length);

        print("****");

        file.Append("E:/Dataset/VOC2012/VOC2012/myImage/clickPattern/2007_000032_3_negClk_s1_1.png");
        mat1 = new cvMat(file);
        mat1.show(23);
        print(mat1.type());
        mat1.Dispose();
        file.Remove(0, file.Length);

        print("End of the line");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
