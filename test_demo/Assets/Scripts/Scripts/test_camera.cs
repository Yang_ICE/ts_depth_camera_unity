﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;
using System;
using System.Text;
using System.Threading;

public class test_camera : MonoBehaviour {

    [DllImport("openCV_320")]
    public static extern void cv_waitKey(int num);

    // Use this for initialization
    void Start () {

        StringBuilder ini_path = new StringBuilder(1024);
        ini_path.Append("C:/Dev/AstraCamera_ini/camera_params.ini");

        AstraCamera cam = new AstraCamera(ini_path, ini_path.Length);

        StringBuilder ini_path_output1 = new StringBuilder(1024);
        cam.get_ini_path(ini_path_output1);
        print(ini_path_output1.ToString());
        ini_path_output1.Remove(0, ini_path_output1.Length);

        StringBuilder ini_path_output2 = new StringBuilder(10);
        cam.get_ini_path(ini_path_output2);
        print(ini_path_output2.ToString());
        ini_path_output2.Remove(0, ini_path_output2.Length);

        cam.get_ini_path(ini_path_output1);
        print(ini_path_output1.ToString());
        ini_path_output1.Remove(0, ini_path_output1.Length);

        cam.get_ini_path(ini_path_output2);
        print(ini_path_output2.ToString());
        ini_path_output2.Remove(0, ini_path_output2.Length);

        cam.get_ini_path(ini_path_output1);
        print(ini_path_output1.ToString());
        ini_path_output1.Remove(0, ini_path_output1.Length);

        cam.get_ini_path(ini_path_output2);
        print(ini_path_output2.ToString());
        ini_path_output2.Remove(0, ini_path_output2.Length);

        cam.get_ini_path(ini_path_output1);
        print(ini_path_output1.ToString());
        ini_path_output1.Remove(0, ini_path_output1.Length);

        cam.get_ini_path(ini_path_output2);
        print(ini_path_output2.ToString());
        ini_path_output2.Remove(0, ini_path_output2.Length);

        cam.get_ini_path(ini_path_output1);
        print(ini_path_output1.ToString());
        ini_path_output1.Remove(0, ini_path_output1.Length);

        cam.get_ini_path(ini_path_output2);
        print(ini_path_output2.ToString());
        ini_path_output2.Remove(0, ini_path_output2.Length);

        cam.start();
        cam.update();

        int r = 0;
        int g = 0;
        int b = 0;

        int x_rgb = 0;
        int y_rgb = 0;

        const int height = 480;
        const int width = 640;

        const int height_resize = 240;
        const int width_resize = 320;

        const double alpha = 0.4;
        const double beta = 1 - alpha;

        double minVal = 0;
        double maxVal = 0;

        double minDepth = 0;
        double maxDepth = 0;

        cvMat tmpColor = new cvMat(height, width, 16, 0);
        cvMat tmpColor1 = new cvMat();
        cvMat tmpDepth = new cvMat(height, width, 21, 0);
        cvMat tmpDepth_Normalize = new cvMat(height, width, 21, 0);
        cvMat tmpDepth_Registered = new cvMat(height, width, 21, 0);
        cvMat tmpDepth_Registered_Normalize = new cvMat(height, width, 21, 0);
        cvMat tmpDepth_vis = new cvMat(height, width, 16, 0);
        cvMat tmpDepth_Registered_vis = new cvMat(height, width, 16, 0);
        cvMat tmpOverlay = new cvMat(height, width, 21, 0);
        cvMat tmpOverlay_vis = new cvMat(height, width, 16, 0);
        cvMat tmpDepth_Registered_Normalize_resize_vis = new cvMat();

        cvMat tmpDepth_Registered_8u = new cvMat();
        cvMat tmpDepth_Registered_resize_8u = new cvMat();
        cvMat mask_zero = new cvMat();
        cvMat mask_zero_resize = new cvMat();
        cvMat tmpRstInpaint = new cvMat(height_resize, width_resize, 0, 0);
        cvMat rstInpaint = new cvMat(height, width, 5, 0);
        cvMat tmpDepthInpaint = new cvMat(height, width, 21, 0);
        cvMat tmpDepthInpaint_normalize = new cvMat(height, width, 21, 0);
        cvMat tmpDepthInpaint_normalize_vis = new cvMat(height, width, 5, 0);

        print(cam.is_paused());
        cam.toggle_pause();
        print(cam.is_paused());
        cam.toggle_pause();
        print(cam.is_paused());

        /*
        //test depth to color mapping and inpainting
        for (int cnt = 0; cnt < 300; cnt++)
        {
            cam.update();
            //print(get_depth_data(pCam, 10000));
            //print(r + ", " + g + ", " + b);
            if (cnt == 100 || cnt == 200)
                cam.toggle_pause();

            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    int d = cam.get_depth_data(w + h * width);
                    tmpDepth.set(h, w, 1, 21, 3, (double)d);
                    tmpDepth.set(h, w, 2, 21, 3, (double)d);
                    //registration begins
                    cam.coordinate_depth_to_color_v2(w, h, d, ref x_rgb, ref y_rgb);
                    //print(x_rgb + "," + y_rgb);
                    if (x_rgb >= 0 && x_rgb < width && y_rgb >= 0 && y_rgb < height)
                    {
                        tmpDepth_Registered.set(y_rgb, x_rgb, 1, 21, 3, (double)d);
                        tmpDepth_Registered.set(y_rgb, x_rgb, 2, 21, 3, (double)d);
                    }
                    //registration ends

                    cam.get_color_data(w + h * width, ref r, ref g, ref b);
                    tmpColor.set(h, w, 0, 16, 3, (double)b);
                    tmpColor.set(h, w, 1, 16, 3, (double)g);
                    tmpColor.set(h, w, 2, 16, 3, (double)r);
                }
            }

            tmpDepth.minMax(ref minVal, ref maxVal);
            tmpDepth_Normalize = tmpDepth.normalize(minVal, maxVal, (double)255.0);
            tmpDepth_vis = tmpDepth_Normalize.convertTo(16);

            tmpDepth_Registered.minMax(ref minVal, ref maxVal);
            //print(minVal + ", " + maxVal);
            minDepth = minVal;
            maxDepth = maxVal;
            tmpDepth_Registered_Normalize = tmpDepth_Registered.normalize(minVal, maxVal, (double)255.0);
            tmpDepth_Registered_vis = tmpDepth_Registered_Normalize.convertTo(16);

            tmpDepth_Registered_Normalize_resize_vis = tmpDepth_Registered_vis.resize(height_resize, width_resize, 0);

            tmpColor1 = tmpColor.convertTo(21);
            tmpOverlay = tmpColor1.addWeighted(alpha, tmpDepth_Registered_Normalize, beta, (double)0.0);

            tmpOverlay_vis = tmpOverlay.convertTo(16);

            //depth inpaint begins
            tmpDepth_Registered_8u = tmpDepth_Registered_Normalize.extractChannel(1);
            tmpDepth_Registered_resize_8u = tmpDepth_Registered_Normalize_resize_vis.extractChannel(1);
            mask_zero = tmpDepth_Registered_8u.isEqual(0);
            mask_zero_resize = mask_zero.resize(height_resize, width_resize, 0);
            tmpRstInpaint = tmpDepth_Registered_resize_8u.inpaint(mask_zero_resize, 3, 1);
            rstInpaint = tmpRstInpaint.convertTo(5).resize(height, width, 1);

            tmpDepthInpaint = tmpDepth_Registered.copy();
            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    double d = tmpDepthInpaint.get(h, w, 1, 21, 3);
                    if (d == 0)
                    {
                        double d_inpaint = (double)(rstInpaint.get(h, w, 0, 5, 1) * (maxDepth - minDepth)) / ((double)255.0) + minVal;
                        tmpDepthInpaint.set(h, w, 1, 21, 3, d_inpaint);
                        tmpDepthInpaint.set(h, w, 2, 21, 3, d_inpaint);
                    }
                }
            }

            tmpDepthInpaint.minMax(ref minVal, ref maxVal);
            tmpDepthInpaint_normalize = tmpDepthInpaint.normalize(minVal, maxVal, (double)255.0);
            tmpDepthInpaint_normalize_vis = tmpDepthInpaint_normalize.convertTo(0);
            //depth inpaint ends

            tmpDepth_Registered.Dispose();
            tmpDepth_Registered = new cvMat(height, width, 21, 0);

            tmpColor.show(0);
            tmpDepth_vis.show(1);
            tmpDepth_Registered_vis.show(2);
            tmpOverlay_vis.show(3);
            tmpDepth_Registered_Normalize_resize_vis.show(4);
            tmpDepthInpaint_normalize_vis.show(5);

            cv_waitKey(5);
        }
        */

        /*
        //test color to depth mapping
        cvMat tmpColor_transfer = new cvMat(height, width, 16, 0);

        int x_d = 0;
        int y_d = 0;

        print(tmpDepth_Registered_Normalize_resize_vis.type());
        for (int cnt = 0; cnt < 300; cnt++)
        {
            cam.update();
            //print(get_depth_data(pCam, 10000));
            //print(r + ", " + g + ", " + b);
            if (cnt == 100 || cnt == 200)
                cam.toggle_pause();

            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    int d = cam.get_depth_data(w + h * width);
                    tmpDepth.set(h, w, 1, 21, 3, (double)d);
                    tmpDepth.set(h, w, 2, 21, 3, (double)d);
                    //registration begins
                    cam.coordinate_depth_to_color_v2(w, h, d, ref x_rgb, ref y_rgb);
                    //print(x_rgb + "," + y_rgb);
                    if (x_rgb >= 0 && x_rgb < width && y_rgb >= 0 && y_rgb < height)
                    {
                        tmpDepth_Registered.set(y_rgb, x_rgb, 1, 21, 3, (double)d);
                        tmpDepth_Registered.set(y_rgb, x_rgb, 2, 21, 3, (double)d);
                    }
                    //registration ends

                    cam.get_color_data(w + h * width, ref r, ref g, ref b);
                    tmpColor.set(h, w, 0, 16, 3, (double)b);
                    tmpColor.set(h, w, 1, 16, 3, (double)g);
                    tmpColor.set(h, w, 2, 16, 3, (double)r);
                }
            }

            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    double d = tmpDepth_Registered.get(h, w, 1, 21, 3);
                    if (d != 0)
                    {
                        cam.get_color_data(w + h * width, ref r, ref g, ref b);
                        cam.coordinate_color_to_depth(w, h, (int)d, ref x_d, ref y_d);
                        if (x_d >= 0 && x_d < width && y_d >= 0 && y_d < height)
                        {
                            tmpColor_transfer.set(y_d, x_d, 0, 16, 3, (double)b);
                            tmpColor_transfer.set(y_d, x_d, 1, 16, 3, (double)g);
                            tmpColor_transfer.set(y_d, x_d, 2, 16, 3, (double)r);
                        }
                    }
                }
            }

            tmpDepth.minMax(ref minVal, ref maxVal);
            tmpDepth_Normalize = tmpDepth.normalize(minVal, maxVal, (double)255.0);

            tmpOverlay = tmpColor_transfer.convertTo(21).addWeighted(alpha, tmpDepth_Normalize, beta, (double)0.0);

            tmpColor.show(0);
            tmpColor_transfer.show(1);
            tmpOverlay.convertTo(16).show(2);
            cv_waitKey(2);

            tmpDepth_Registered.Dispose();
            tmpDepth_Registered = new cvMat(height, width, 21, 0);

            tmpColor_transfer.Dispose();
            tmpColor_transfer = new cvMat(height, width, 16, 0);
        }
        */

        //test coordinate mapping
        for (int cnt = 0; cnt < 300; cnt++)
        {
            cam.update();
            //print(get_depth_data(pCam, 10000));
            //print(r + ", " + g + ", " + b);
            if (cnt == 100 || cnt == 200)
                cam.toggle_pause();

            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    int d = cam.get_depth_data(w + h * width);
                    tmpDepth.set(h, w, 1, 21, 3, (double)d);
                    tmpDepth.set(h, w, 2, 21, 3, (double)d);
                    //registration begins
                    cam.coordinate_depth_to_color_v2(w, h, d, ref x_rgb, ref y_rgb);
                    //print(x_rgb + "," + y_rgb);
                    if (x_rgb >= 0 && x_rgb < width && y_rgb >= 0 && y_rgb < height)
                    {
                        tmpDepth_Registered.set(y_rgb, x_rgb, 1, 21, 3, (double)d);
                        tmpDepth_Registered.set(y_rgb, x_rgb, 2, 21, 3, (double)d);
                    }
                    //registration ends

                    cam.get_color_data(w + h * width, ref r, ref g, ref b);
                    tmpColor.set(h, w, 0, 16, 3, (double)b);
                    tmpColor.set(h, w, 1, 16, 3, (double)g);
                    tmpColor.set(h, w, 2, 16, 3, (double)r);
                }
            }

            tmpDepth_Registered.minMax(ref minVal, ref maxVal);
            tmpDepth_Registered_Normalize = tmpDepth_Registered.normalize(minVal, maxVal, (double)255.0);
            tmpDepth_Registered_vis = tmpDepth_Registered_Normalize.convertTo(16);

            int x_middle = 320;
            int y_middle = 240;
            int depth_middle = (int)tmpDepth_Registered.get(x_middle, y_middle, 1, 21, 3);

            float x_world = 0;
            float y_world = 0;

            cam.coordinate_to_world(x_middle, y_middle, depth_middle, ref x_world, ref y_world);

            print(x_world + ", " + y_world + ", " + depth_middle);

            tmpDepth_Registered_vis.show(0);
            cv_waitKey(2);

            tmpDepth_Registered.Dispose();
            tmpDepth_Registered = new cvMat(height, width, 21, 0);
        }

        tmpDepth_Registered_8u.Dispose();
        tmpDepth_Registered_resize_8u.Dispose();
        mask_zero.Dispose();
        mask_zero_resize.Dispose();
        tmpRstInpaint.Dispose();
        rstInpaint.Dispose();
        tmpDepthInpaint.Dispose();
        tmpDepthInpaint_normalize.Dispose();
        tmpDepthInpaint_normalize_vis.Dispose();

        tmpDepth_Registered_Normalize_resize_vis.Dispose();
        tmpOverlay.Dispose();
        tmpOverlay_vis.Dispose();
        tmpColor.Dispose();
        tmpDepth.Dispose();
        tmpDepth_Normalize.Dispose();
        tmpDepth_Registered.Dispose();
        tmpDepth_Registered_Normalize.Dispose();
        tmpDepth_vis.Dispose();
        tmpDepth_Registered_vis.Dispose();

        cam.terminate();
        cam.Dispose();

        print("end of the line");
	}    

	// Update is called once per frame
	void Update () {
		
	}
}
