﻿using System.Runtime.InteropServices;
using System;
using System.Text;

public class CaffeNet : IDisposable
{
    [DllImport("caffe_net")]
    public static extern IntPtr CreateNet();

    [DllImport("caffe_net")]
    public static extern void DisposeNet(IntPtr net);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_mode(IntPtr net, int mode);

    [DllImport("caffe_net")]
    public static extern bool caffe_net_check_device(IntPtr net, int id);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_device(IntPtr net, int id);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_model(IntPtr net, StringBuilder model, int len);

    [DllImport("caffe_net")]
    public static extern void caffe_net_get_model(IntPtr net, StringBuilder model, int capacity);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_weight(IntPtr net, StringBuilder weight_file, int len);

    [DllImport("caffe_net")]
    public static extern void caffe_net_get_weight_file(IntPtr net, StringBuilder weight_file, int capacity);

    [DllImport("caffe_net")]
    public static extern void caffe_net_get_input_size(IntPtr net, ref int height, ref int width, ref int channel, ref int number);

    [DllImport("caffe_net")]
    public static extern void caffe_net_input_reshape(IntPtr net, int height, int width, int channel, int number);

    [DllImport("caffe_net")]
    public static extern void caffe_net_bridge_mutable_input_data(IntPtr net);

    [DllImport("caffe_net")]
    public static extern void caffe_net_set_input_data(IntPtr net, int idx, double val);

    [DllImport("caffe_net")]
    public static extern double caffe_net_get_input_data(IntPtr net, int idx);

    [DllImport("caffe_net")]
    public static extern void caffe_net_bridge_output_data(IntPtr net);

    [DllImport("caffe_net")]
    public static extern double caffe_net_get_output_data(IntPtr net, int idx);

    [DllImport("caffe_net")]
    public static extern void caffe_net_forward_pass(IntPtr net);

    private IntPtr pObj;

    public CaffeNet()
    {
        this.pObj = CreateNet();
    }

    public void Dispose()
    {
        Dispose(true);
    }

    protected virtual void Dispose(bool bDisposing)
    {
        if (this.pObj != IntPtr.Zero)
        {
            // Call the DLL Export to dispose this class
            DisposeNet(this.pObj);
            this.pObj = IntPtr.Zero;
        }

        if (bDisposing)
        {
            // No need to call the finalizer since we've now cleaned
            // up the unmanaged memory
            GC.SuppressFinalize(this);
        }
    }

    // This finalizer is called when Garbage collection occurs, but only if
    // the IDisposable.Dispose method wasn't already called.
    ~CaffeNet()
    {
        Dispose(false);
    }

    public void set_mode(int mode)
    {
        caffe_net_set_mode(this.pObj, mode);
    }

    public bool check_device(int id)
    {
        return caffe_net_check_device(this.pObj, id);
    }

    public void set_device(int id)
    {
        caffe_net_set_device(this.pObj, id);
    }

    public void set_model(StringBuilder model, int len)
    {
        caffe_net_set_model(this.pObj, model, len);
    }

    public void get_model(StringBuilder model)
    {
        caffe_net_get_model(this.pObj, model, model.Capacity);
    }

    public void set_weight(StringBuilder weight_file, int len)
    {
        caffe_net_set_weight(this.pObj, weight_file, len);
    }

    public void get_weight_file(StringBuilder weight_file)
    {
        caffe_net_get_weight_file(this.pObj, weight_file, weight_file.Capacity);
    }

    public void get_input_size(ref int height, ref int width, ref int channel, ref int number)
    {
        caffe_net_get_input_size(this.pObj, ref height, ref width, ref channel, ref number);
    }

    public void input_reshape(int height, int width, int channel, int number)
    {
        caffe_net_input_reshape(this.pObj, height, width, channel, number);
    }

    public void bridge_mutable_input_data()
    {
        caffe_net_bridge_mutable_input_data(this.pObj);
    }

    public void set_input_data(int idx, double val)
    {
        caffe_net_set_input_data(this.pObj, idx, val);
    }

    public double get_input_data(int idx)
    {
        return caffe_net_get_input_data(this.pObj, idx);
    }    

    public void bridge_output_data()
    {
        caffe_net_bridge_output_data(this.pObj);
    }

    public double get_output_data(int idx)
    {
        return caffe_net_get_output_data(this.pObj, idx);
    }

    public void forward()
    {
        caffe_net_forward_pass(this.pObj);
    }
}