﻿using System.Runtime.InteropServices;
using System;
using System.Text;

public class cvMat : IDisposable
{
    [DllImport("openCV_320")]
    public static extern IntPtr CreateMat();

    [DllImport("openCV_320")]
    public static extern int cv_Mat_type(IntPtr pObject);

    [DllImport("openCV_320")]
    public static extern void DisposeMat(IntPtr pObject);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_Ones(int height, int width, int type);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_Zeros(int height, int width, int type);

    [DllImport("openCV_320")]
    public static extern double cv_Mat_get_C1(IntPtr pObject, int i, int j, int type);

    [DllImport("openCV_320")]
    public static extern double cv_Mat_get_C3(IntPtr pObject, int i, int j, int c, int type);

    [DllImport("openCV_320")]
    public static extern void cv_Mat_set_C1(IntPtr pObject, int i, int j, int type, double val);

    [DllImport("openCV_320")]
    public static extern void cv_Mat_set_C3(IntPtr pObject, int i, int j, int channel, int type, double val);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_isLess(IntPtr pObject, double num);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_isGreater(IntPtr pObject, double num);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_isEqual(IntPtr pObject, double num);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_isNotEqual(IntPtr pObject, double num);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_convertTo(IntPtr pObject, int type);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_copy(IntPtr pObject);

    [DllImport("openCV_320")]
    public static extern void cv_Mat_setTo(IntPtr pObject, double val, IntPtr mask);

    [DllImport("openCV_320")]
    public static extern void cv_imshow(int id, IntPtr pObject);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_normalize(IntPtr pObject, double minVal, double maxVal, double scale);

    [DllImport("openCV_320")]
    public static extern void cv_Mat_minMax(IntPtr pObject, ref double minVal, ref double maxVal);

    [DllImport("openCV_320")]
    public static extern void cv_Mat_minMax_mask(IntPtr pObject, ref double minVal, ref double maxVal, IntPtr mask);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_addWeighted(IntPtr pObject1, double alpha, IntPtr pObject2, double beta, double gamma);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_subtract_scalar(IntPtr pObject, double num);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_multiply(IntPtr pObject1, IntPtr pObject2);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_multiply_scalar(IntPtr pObject, double num);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_add(IntPtr pObject1, IntPtr pObject2);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_add_scalar(IntPtr pObject1, double num);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_divide_scalar(double numerator, IntPtr pObject1);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_divide(IntPtr pObject1, IntPtr pObject2);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_sqrt(IntPtr pObject);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_min(IntPtr pObject1, IntPtr pObject2);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_exp(IntPtr pObject);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_bitwise_and(IntPtr pObject1, IntPtr pObject2);

    [DllImport("openCV_320")]
    public static extern IntPtr cvtColor_BGR2LAB(IntPtr pObject);

    [DllImport("openCV_320")]
    public static extern IntPtr cvtColor_RGB2LAB(IntPtr pObject);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_Mat_extractChannel(IntPtr pObject, int channel);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_imread(StringBuilder file, int len);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_resize(IntPtr pObjecte, int height, int width, int mode);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_inpaint(IntPtr pObjecte, IntPtr pMask, int radius, int mode);

    [DllImport("openCV_320")]
    public static extern IntPtr get_polygon(IntPtr pObjecte, int width);

    protected internal IntPtr pObj;

    public cvMat()
    {
        this.pObj = CreateMat();
    }

    public cvMat(int height, int width, int type, int val)
    {
        if (val == 0)
            this.pObj = cv_Mat_Zeros(height, width, type);
        else
            this.pObj = cv_Mat_Ones(height, width, type);
    }

    public cvMat(StringBuilder file)
    {
        this.pObj = cv_imread(file, file.Length);
    }

    public void Dispose()
    {
        Dispose(true);
    }

    protected virtual void Dispose(bool bDisposing)
    {
        if (this.pObj != IntPtr.Zero)
        {
            // Call the DLL Export to dispose this class
            DisposeMat(this.pObj);
            this.pObj = IntPtr.Zero;
        }

        if (bDisposing)
        {
            // No need to call the finalizer since we've now cleaned
            // up the unmanaged memory
            GC.SuppressFinalize(this);
        }
    }

    // This finalizer is called when Garbage collection occurs, but only if
    // the IDisposable.Dispose method wasn't already called.
    ~cvMat()
    {
        Dispose(false);
    }   
    
    public int type()
    {
        return cv_Mat_type(this.pObj);
    } 

    public double get(int i, int j, int channel, int type, int numChannel)
    {
        if (numChannel == 3)
            return cv_Mat_get_C3(this.pObj, i, j, channel, type);
        else
            return cv_Mat_get_C1(this.pObj, i, j, type);
    }

    public void set(int i, int j, int channel, int type, int numChannel, double val)
    {
        if (numChannel == 3)
            cv_Mat_set_C3(this.pObj, i, j, channel, type, val);
        else
            cv_Mat_set_C1(this.pObj, i, j, type, val);
    }

    public cvMat copy()
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_copy(this.pObj);
        return rst;
    }

    public void show(int id)
    {
        cv_imshow(id, this.pObj);
    }

    public cvMat isLess(double num)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_isLess(this.pObj, num);
        return rst;
    }

    public cvMat isGreater(double num)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_isGreater(this.pObj, num);
        return rst;
    }

    public cvMat isEqual(double num)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_isEqual(this.pObj, num);
        return rst;
    }

    public cvMat isNotEqual(double num)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_isNotEqual(this.pObj, num);
        return rst;
    }

    public void setTo_mask(double val, cvMat mask)
    {
        cv_Mat_setTo(this.pObj, val, mask.pObj);
    }

    public cvMat convertTo(int type)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_convertTo(this.pObj, type);
        return rst;
    }

    public cvMat normalize(double minVal, double maxVal, double scale)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_normalize(this.pObj, minVal, maxVal, scale);
        return rst;
    }

    public void minMax(ref double minVal, ref double maxVal)
    {
        cv_Mat_minMax(this.pObj, ref minVal, ref maxVal);
    }

    public void minMax_mask(ref double minVal, ref double maxVal, cvMat mask)
    {
        cv_Mat_minMax_mask(this.pObj, ref minVal, ref maxVal, mask.pObj);
    }

    public cvMat addWeighted(double alpha, cvMat mat, double beta, double gamma)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_addWeighted(this.pObj, alpha, mat.pObj, beta, gamma);
        return rst;
    }

    public cvMat subtract_scalar(double num)
    {
        cvMat rst = new cvMat();
        rst.pObj= cv_Mat_subtract_scalar(this.pObj, num);
        return rst;
    }

    public cvMat multiply(cvMat mat)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_multiply(this.pObj, mat.pObj);
        return rst;
    }

    public cvMat multiply_scalar(double num)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_multiply_scalar(this.pObj, num);
        return rst;
    }

    public cvMat add(cvMat mat)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_add(this.pObj, mat.pObj);
        return rst;
    }
    public cvMat add_scalar(double num)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_add_scalar(this.pObj, num);
        return rst;
    }

    public cvMat divide_scalar(double numerator)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_divide_scalar(numerator, this.pObj);
        return rst;
    }

    public cvMat divide(cvMat mat_denominator)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_divide(this.pObj, mat_denominator.pObj);
        return rst;
    }

    public cvMat sqrt()
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_sqrt(this.pObj);
        return rst;
    }

    public cvMat min(cvMat mat)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_min(this.pObj, mat.pObj);
        return rst;
    }

    public cvMat exp()
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_exp(this.pObj);
        return rst;
    }

    public cvMat bitwise_and(cvMat mat)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_bitwise_and(this.pObj, mat.pObj);
        return rst;
    }

    public cvMat cvtColor_BGR2LAB()
    {
        cvMat rst = new cvMat();
        rst.pObj = cvtColor_BGR2LAB(this.pObj);
        return rst;
    }

    public cvMat cvtColor_RGB2LAB()
    {
        cvMat rst = new cvMat();
        rst.pObj = cvtColor_RGB2LAB(this.pObj);
        return rst;
    }

    public cvMat extractChannel(int channel)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_Mat_extractChannel(this.pObj, channel);
        return rst;
    }

    public cvMat resize(int height, int width, int mode)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_resize(this.pObj, height, width, mode);
        return rst;
    }

    public cvMat inpaint(cvMat mask, int radius, int mode)
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_inpaint(this.pObj, mask.pObj, radius, mode);
        return rst;
    }

    public Polygon getPolygon(int width)
    {
        return new Polygon(get_polygon(this.pObj, width));
    }
}

public class cvMeshGrid : cvMat //this is a simple and less standard version, can be extended and improved in the future
{
    [DllImport("openCV_320")]
    public static extern IntPtr cv_meshgrid_getX(int rangeX_l, int rangeX_h, int rangeY_l, int rangeY_h);

    [DllImport("openCV_320")]
    public static extern IntPtr cv_meshgrid_getY(int rangeX_l, int rangeX_h, int rangeY_l, int rangeY_h);    

    private int rangeX_l;
    private int rangeX_h;
    private int rangeY_l;
    private int rangeY_h;

    public cvMeshGrid()
    {
        this.pObj = IntPtr.Zero;
    }

    public cvMeshGrid(int rangeX_l, int rangeX_h, int rangeY_l, int rangeY_h)
    {
        this.rangeX_l = rangeX_l;
        this.rangeX_h = rangeX_h;
        this.rangeY_l = rangeY_l;
        this.rangeY_h = rangeY_h;
        this.pObj = IntPtr.Zero;
    }

    public void get_rangeX(ref int l, ref int h)
    {
        l = this.rangeX_l;
        h = this.rangeX_h;
    }

    public void get_rangeY(ref int l, ref int h)
    {
        l = this.rangeY_l;
        h = this.rangeY_h;
    }

    public void set_rangeX(int l, int h)
    {
        this.rangeX_l = l;
        this.rangeX_h = h;
    }

    public void set_rangeY(int l, int h)
    {
        this.rangeY_l = l;
        this.rangeY_h = h;
    }

    public cvMat get_meshX()
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_meshgrid_getX(rangeX_l, rangeX_h, rangeY_l, rangeY_h);
        return rst;
    }

    public cvMat get_meshY()
    {
        cvMat rst = new cvMat();
        rst.pObj = cv_meshgrid_getY(rangeX_l, rangeX_h, rangeY_l, rangeY_h);
        return rst;
    }

    public void get_mesh(ref cvMat meshX, ref cvMat meshY)
    {
        meshX.pObj = cv_meshgrid_getX(rangeX_l, rangeX_h, rangeY_l, rangeY_h);
        meshY.pObj = cv_meshgrid_getY(rangeX_l, rangeX_h, rangeY_l, rangeY_h);
    }
}

public class Polygon : IDisposable
{
    [DllImport("openCV_320")]
    public static extern IntPtr CreatePolygon();

    [DllImport("openCV_320")]
    public static extern void DisposePolygon(IntPtr pObj);

    [DllImport("openCV_320")]
    public static extern int get_size_polygon(IntPtr pObj);    

    [DllImport("openCV_320")]
    public static extern void get_vertex_polygon(IntPtr pObj, int idx, ref double x, ref double y);

    [DllImport("openCV_320")]
    public static extern IntPtr get_min_quad(IntPtr pObj);

    [DllImport("openCV_320")]
    public static extern void add_vertex_polygon(IntPtr pObj, double x, double y);

    private IntPtr pObj;

    public Polygon()
    {
        this.pObj = CreatePolygon();
    }

    public Polygon(IntPtr pObj)
    {
        this.pObj = pObj;
    }

    public void Dispose()
    {
        Dispose(true);
    }

    protected virtual void Dispose(bool bDisposing)
    {
        if (this.pObj != IntPtr.Zero)
        {
            // Call the DLL Export to dispose this class
            DisposePolygon(this.pObj);
            this.pObj = IntPtr.Zero;
        }

        if (bDisposing)
        {
            // No need to call the finalizer since we've now cleaned
            // up the unmanaged memory
            GC.SuppressFinalize(this);
        }
    }

    ~Polygon()
    {
        Dispose(false);
    }

    public int size()
    {
        return get_size_polygon(this.pObj);
    }

    public void get_vertex(int idx, ref double x, ref double y)
    {
        get_vertex_polygon(pObj, idx, ref x, ref y);
    }

    public Polygon get_min_quad()
    {
        Polygon rst = new Polygon(get_min_quad(this.pObj));
        return rst;
    }

    public void add_vertex(double x, double y)
    {
        add_vertex_polygon(this.pObj, x, y);
    }

    public bool is_empty()
    {
        return this.pObj == IntPtr.Zero;
    }
}
