﻿using System.Runtime.InteropServices;
using System;

public class GraphCutOptimizationGeneralGraph_build_without_source : IDisposable
{
    [DllImport("gco_wrapper_without_source")]
    public static extern IntPtr Create(int num_sites, int num_labels);

    [DllImport("gco_wrapper_without_source")]
    public static extern void DisposeObj(IntPtr pObject);

    [DllImport("gco_wrapper_without_source")]
    public static extern void call_setDataCost(IntPtr pObject, int[] data);

    [DllImport("gco_wrapper_without_source")]
    public static extern void call_setSmoothCost(IntPtr pObject, int[] data);

    [DllImport("gco_wrapper_without_source")]
    public static extern void call_setNeighbors(IntPtr pObject, int site1, int site2, int weight);

    [DllImport("gco_wrapper_without_source")]
    public static extern int call_compute_energy(IntPtr pObject);

    [DllImport("gco_wrapper_without_source")]
    public static extern int call_whatLabel(IntPtr pObject, int site);

    [DllImport("gco_wrapper_without_source")]
    public static extern void call_expansion(IntPtr pObject, int iter);

    private IntPtr pObj;

    public GraphCutOptimizationGeneralGraph_build_without_source(int num_sites, int num_labels)
    {
        this.pObj = Create(num_sites, num_labels);
    }

    public void Dispose()
    {
        Dispose(true);
    }

    protected virtual void Dispose(bool bDisposing)
    {
        if (this.pObj != IntPtr.Zero)
        {
            // Call the DLL Export to dispose this class
            DisposeObj(this.pObj);
            this.pObj = IntPtr.Zero;
        }

        if (bDisposing)
        {
            // No need to call the finalizer since we've now cleaned
            // up the unmanaged memory
            GC.SuppressFinalize(this);
        }
    }

    // This finalizer is called when Garbage collection occurs, but only if
    // the IDisposable.Dispose method wasn't already called.
    ~GraphCutOptimizationGeneralGraph_build_without_source()
    {
        Dispose(false);
    }

    public void setDataCost(int[] data)
    {
        call_setDataCost(this.pObj, data);
    }

    public void setSmoothCost(int[] data)
    {
        call_setSmoothCost(this.pObj, data);
    }

    public void setNeighbors(int site1, int site2, int weight)
    {
        call_setNeighbors(this.pObj, site1, site2, weight);
    }

    public int compute_energy()
    {
        return call_compute_energy(this.pObj);
    }

    public int whatLabel(int site)
    {
        return call_whatLabel(this.pObj, site);
    }

    public void expansion(int iter)
    {
        call_expansion(this.pObj, iter);
    }
}