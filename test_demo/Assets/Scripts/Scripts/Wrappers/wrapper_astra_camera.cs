﻿using System.Runtime.InteropServices;
using System;
using System.Text;

public class AstraCamera : IDisposable
{
    [DllImport("astra_camera")]
    public static extern IntPtr CreateAstraCamera(StringBuilder ini_path, int len);

    [DllImport("astra_camera")]
    public static extern void DisposeAstraCamera(IntPtr pCamera);

    [DllImport("astra_camera")]
    public static extern void start(IntPtr pCamera);

    [DllImport("astra_camera")]
    public static extern void update(IntPtr pCamera);

    [DllImport("astra_camera")]
    public static extern int get_depth_data(IntPtr pCamera, int idx);

    [DllImport("astra_camera")]
    public static extern void get_color_data(IntPtr pCamera, int idx, ref int r, ref int g, ref int b);

    [DllImport("astra_camera")]
    public static extern void InitializeCamera();

    [DllImport("astra_camera")]
    public static extern void TerminateCamera();

    [DllImport("astra_camera")]
    public static extern void coordinate_depth_to_color_v2(IntPtr pCamera, int x, int y, int d, ref int x_rgb, ref int y_rgb);

    [DllImport("astra_camera")]
    public static extern void coordinate_color_to_depth(IntPtr pCamera, int x, int y, int d, ref int x_d, ref int y_d);

    [DllImport("astra_camera")]
    public static extern void coordinate_to_world(IntPtr pCamera, int x, int y, int d, ref float x_world, ref float y_world);

    [DllImport("astra_camera")]
    public static extern bool is_paused(IntPtr pCamera);

    [DllImport("astra_camera")]
    public static extern void toggle_paused(IntPtr pCamera);

    [DllImport("astra_camera")]
    public static extern void get_ini_path(IntPtr pCamera, StringBuilder ini_path, int capacity);

    private IntPtr pObj;

    public AstraCamera(StringBuilder ini_path, int len)
    {
        InitializeCamera();
        this.pObj = CreateAstraCamera(ini_path, len);
    }

    public void Dispose()
    {
        Dispose(true);
    }

    protected virtual void Dispose(bool bDisposing)
    {
        if (this.pObj != IntPtr.Zero)
        {
            // Call the DLL Export to dispose this class
            DisposeAstraCamera(this.pObj);
            this.pObj = IntPtr.Zero;
        }

        if (bDisposing)
        {
            // No need to call the finalizer since we've now cleaned
            // up the unmanaged memory
            GC.SuppressFinalize(this);
        }
    }

    // This finalizer is called when Garbage collection occurs, but only if
    // the IDisposable.Dispose method wasn't already called.
    ~AstraCamera()
    {
        Dispose(false);
    }

    public void start()
    {
        start(this.pObj);
    }

    public void update()
    {
        update(this.pObj);
    }

    public int get_depth_data(int idx)
    {
        return get_depth_data(this.pObj, idx);
    }

    public void get_color_data(int idx, ref int r, ref int g, ref int b)
    {
        get_color_data(this.pObj, idx, ref r, ref g, ref b);
    }

    public void terminate()
    {
        TerminateCamera();
    }

    public void coordinate_depth_to_color_v2(int x, int y, int d, ref int x_rgb, ref int y_rgb)
    {
        coordinate_depth_to_color_v2(this.pObj, x, y, d, ref x_rgb, ref y_rgb);
    }

    public void coordinate_color_to_depth(int x, int y, int d, ref int x_d, ref int y_d)
    {
        coordinate_color_to_depth(this.pObj, x, y, d, ref x_d, ref y_d);
    }

    public void coordinate_to_world(int x, int y, int d, ref float x_world, ref float y_world)
    {
        coordinate_to_world(this.pObj, x, y, d, ref x_world, ref y_world);
    }

    public bool is_paused()
    {
        return is_paused(this.pObj);
    }

    public void toggle_pause()
    {
        toggle_paused(this.pObj);
    }

    public void get_ini_path(StringBuilder ini_path)
    {
        get_ini_path(this.pObj, ini_path, ini_path.Capacity);
    }
}
