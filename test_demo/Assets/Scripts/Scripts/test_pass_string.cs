﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System.Text;

public class test_pass_string : MonoBehaviour {

    //[DllImport("testCharString_dll", CharSet = CharSet.Ansi)]
    [DllImport("testCharString_dll")]
    public static extern int testCharToInt(StringBuilder strIn, int length);

    //[DllImport("testCharString_dll", CharSet = CharSet.Ansi)]
    [DllImport("testCharString_dll")]
    public static extern int testCharToChar(StringBuilder strIn, int length, StringBuilder strOut);

    // Use this for initialization
    void Start () {
        StringBuilder result = new StringBuilder(500);
        result.Append("aaa");

        print("***************************");

        int val;

        val = testCharToInt(result, result.Length);

        print("val: " + val);

        result.Remove(0, result.Length);

        result.Append("aba");

        val = testCharToInt(result, result.Length);

        print("val: " + val);

        result.Remove(0, result.Length);

        result.Append("abc");

        val = testCharToInt(result, result.Length);

        print("val: " + val);

        result.Remove(0, result.Length);

        result.Append("abcdefg");

        val = testCharToInt(result, result.Length);

        print("val: " + val);

        result.Remove(0, result.Length);

        print("***************************");

        result.Append("abcdefgh");
        StringBuilder result_out = new StringBuilder(500);
        testCharToChar(result, result.Length, result_out);
        print(result_out.ToString());
        result.Remove(0, result.Length);
        result_out.Remove(0, result.Length);

        result.Append("ejh/df");
        testCharToChar(result, result.Length, result_out);
        print(result_out.ToString());
        result.Remove(0, result.Length);
        result_out.Remove(0, result_out.Length);

        result.Append("c:/asds/");
        testCharToChar(result, result.Length, result_out);
        print(result_out.ToString());
        result.Remove(0, result.Length);
        result_out.Remove(0, result_out.Length);

        print("End of the line");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
