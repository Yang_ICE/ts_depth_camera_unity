﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/*
CV_8UC1 0
CV_8UC3 16
CV_32FC1 5
CV_32FC3 21
CV_64FC1 6
CV_64FC3 22
*/

public class test_demo : MonoBehaviour
{

    private Texture2D view;
    private Texture2D view_rst;
    //private Texture2D view_seg_rst;
    private Color color_rgb;
    //private Color color_rgba;
    private ColorBlock colors_button_on;
    private ColorBlock colors_button_off;
    private Color color_button_register_normal_color;
    private const int input_height = 480;
    private const int input_width = 640;
    private RawImage img;
    private RawImage img_rst;
    //private RawImage img_seg_rst;
    private int w_start = 300;
    private int h_start = 20;
    private AstraCamera cam;
    private int r = 0;
    private int g = 0;
    private int b = 0;
    private int depth = 0;
    private float depth_normalize = 0;
    private double minDepth;
    private double maxDepth;

    private bool is_color = true;
    private bool is_register = true;
    private bool is_clear_view = false;

    private int x_unity = 0;
    private int y_unity = 0;

    private int x_register = 0;
    private int y_register = 0;

    private float view_height;
    private float view_width;

    private cvMat im_bgr;
    private cvMat im_depth;
    private cvMat im_depth_register;
    private cvMat im_depth_normalize;
    private cvMat im_depth_inpaint;
    private cvMat im_depth_inpaint_normalize;
    private cvMat im_pos_interaction;
    private cvMat im_neg_interaction;
    private cvMat im_pos_interaction_prev;
    private cvMat im_neg_interaction_prev;
    private cvMat meshX;
    private cvMat meshY;
    private cvMat im_prob_map;
    private cvMat im_seg_rst;

    private const double pi = 3.1415926;

    private int mode = 0;

    private List<Vector2> posClick;
    private List<Vector2> posMarker;
    private List<Vector3> posWorld;
    private List<float> distance;
    private List<GameObject> textSet;
    //private int cnt_pos_marker = 0;

    private List<List<Vector2>> posInteraction;
    private List<List<Vector2>> negInteraction;
    private List<Vector2> currentInteraction;

    private bool is_positive_stroke = true;

    private CaffeNet net;

    private bool is_gpu = true;

    //graph cut variables begin
    private int numNeighbour;
    private int numSite;
    private int numLabel;

    private GraphCutOptimizationGeneralGraph gCut; //initialized when pause the capture

    private int[] unary_data;
    private int[] smooth;
    double[,] cost;

    private bool draw_seg = false;
    private Polygon poly_contour;
    private Polygon poly_min_quad;

    private bool is_manual_bounding;
    //private int cnt_manual_bounding;

    private List<List<double>> line_params;

    private List<int> idxSelSet_manual_bound;

    public float sigma;
    public float scale;
    public float lambda;
    //graph cut variables end

    public string camIniPath;
    public string ModelPath;
    public string WeightPath;

    public GameObject ImageOnPanel;
    public GameObject ImageOnPanel1;
    //public GameObject ImageOnPanelSegRst;    
    public Button ButtonRegister;
    public Button ButtonClearView;
    public Button ButtonChangeMode;
    public GameObject ButtonStrokeType;
    public GameObject ButtonCaffeMode;
    public GameObject ButtonDoSegmentation;
    public GameObject ButtonCancelCurrentStroke;
    public GameObject ButtonAutoBound;
    public GameObject ButtonManualBound;
    public Camera canvasCamera;

    public int circle_inner_radius;
    public int circle_outer_radius;
    public float line_width;
    public float stroke_width;    
    public int dot_line_length;
    public double text_measure_shift;

    public int stroke_effect_radius;

    public double height_measure_quad;    

    public double mean_b;
    public double mean_g;
    public double mean_r;
    public double mean_pos;
    public double mean_neg;

    public double color_weight_overlay;

    public int width_morphology;    

    void Awake()
    {
        Application.targetFrameRate = 30;
    }

    // Use this for initialization
    void Start()
    {

        //Set color of buttons
        colors_button_off = ButtonRegister.colors;        
        colors_button_on = ButtonRegister.colors;
        colors_button_on.normalColor = new Color(colors_button_on.pressedColor.r, colors_button_on.pressedColor.g, colors_button_on.pressedColor.b, colors_button_on.pressedColor.a);
        colors_button_on.highlightedColor = colors_button_off.pressedColor;

        //Initialize camera
        StringBuilder cam_ini_path = new StringBuilder(1024);
        cam_ini_path.Append(camIniPath);
        cam = new AstraCamera(cam_ini_path, cam_ini_path.Length);
        print(cam_ini_path.ToString());
        //StringBuilder ini_path_output2 = new StringBuilder(1024);
        //cam.get_ini_path(ini_path_output2);
        //print(ini_path_output2.ToString());
        cam.start();

        //Initialize camera view
        view = new Texture2D(input_width, input_height);
        img = (RawImage)ImageOnPanel.GetComponent<RawImage>();

        view_rst = new Texture2D(input_width, input_height);
        img_rst = (RawImage)ImageOnPanel1.GetComponent<RawImage>();

        color_rgb = Color.black;
        for (int h = 0; h < input_height; h++)
            for (int w = 0; w < input_width; w++)
            {
                view.SetPixel(w, h, color_rgb);
                view_rst.SetPixel(w, h, color_rgb);
            }
        img.texture = (Texture)view;
        img_rst.texture = (Texture)view_rst;

        view.Apply();
        view_rst.Apply();

        /*
        //view of seg rst
        view_seg_rst = new Texture2D(input_width, input_height);
        img_seg_rst = (RawImage)ImageOnPanelSegRst.GetComponent<RawImage>();

        color_rgba = new Color(1.0F, 1.0F, 1.0F, 0.0F);
        for (int h = 0; h < input_height; h++)
            for (int w = 0; w < input_width; w++)
            {
                view_seg_rst.SetPixel(w, h, color_rgba);
                view_seg_rst.SetPixel(w, h, color_rgba);
            }
        img_seg_rst.texture = (Texture)view_seg_rst;
        view_seg_rst.Apply();
        */

        //Get view height and width
        view_height = img.rectTransform.rect.height;
        view_width = img.rectTransform.rect.width;

        //Initial mode is measure; deactive buttons for segmentation mode
        ButtonChangeMode.enabled = false; //Initial setting of change mode button is disabled
        ButtonStrokeType.SetActive(false);
        ButtonCaffeMode.SetActive(false);
        ButtonDoSegmentation.SetActive(false);
        ButtonCancelCurrentStroke.SetActive(false);
        ButtonAutoBound.SetActive(false);
        ButtonManualBound.SetActive(false);

        //Initialize matrix to buffer color and depth capture
        im_bgr = new cvMat(input_height, input_width, 16, 0);
        im_depth = new cvMat(input_height, input_width, 5, 0);
        im_depth_register = new cvMat(input_height, input_width, 5, 0);
        im_depth_normalize = new cvMat(input_height, input_width, 5, 0);
        im_depth_inpaint = new cvMat(input_height, input_width, 5, 0);
        im_depth_inpaint_normalize = new cvMat(input_height, input_width, 5, 0);
        im_pos_interaction = (new cvMat(input_height, input_width, 6, 1)).multiply_scalar(255.0);
        im_neg_interaction = (new cvMat(input_height, input_width, 6, 1)).multiply_scalar(255.0);
        im_pos_interaction_prev = (new cvMat(input_height, input_width, 6, 1)).multiply_scalar(255.0);
        im_neg_interaction_prev = (new cvMat(input_height, input_width, 6, 1)).multiply_scalar(255.0);
        cvMeshGrid mesh = new cvMeshGrid(0, input_width - 1, 0, input_height - 1);
        meshX = new cvMat();
        meshY = new cvMat();
        mesh.get_mesh(ref meshX, ref meshY);
        mesh.Dispose();

        im_prob_map = new cvMat(input_height, input_width, 5, 0);
        im_seg_rst = new cvMat(input_height, input_width, 0, 0);

        //Debug.Log(im_pos_interaction.get(100, 100, 0, 6, 1));
        //Debug.Log(im_neg_interaction.get(200, 300, 0, 6, 1));

        //Debug.Log(meshX.get(0, 0, 0, 6, 1));
        //Debug.Log(meshX.get(0, input_width - 1, 0, 6, 1));
        //Debug.Log(meshX.get(input_height - 1, 0, 0, 6, 1));

        //Debug.Log(meshY.get(0, 0, 0, 6, 1));
        //Debug.Log(meshY.get(0, input_width - 1, 0, 6, 1));
        //Debug.Log(meshY.get(input_height - 1, 0, 0, 6, 1));

        //Initialize caffe
        net = new CaffeNet();
        net.set_mode(1);
        net.set_device(0);

        StringBuilder tmpPath = new StringBuilder(1024);

        tmpPath.Append(ModelPath);
        net.set_model(tmpPath, tmpPath.Length);
        tmpPath.Remove(0, tmpPath.Length);

        tmpPath.Append(WeightPath);
        net.set_weight(tmpPath, tmpPath.Length);
        tmpPath.Remove(0, tmpPath.Length);

        net.get_model(tmpPath);
        Debug.Log(tmpPath.ToString());
        tmpPath.Remove(0, tmpPath.Length);

        net.get_weight_file(tmpPath);
        Debug.Log(tmpPath.ToString());
        tmpPath.Remove(0, tmpPath.Length);

        net.input_reshape(input_height, input_width, 5, 1);

        //Set graph cut variables
        numNeighbour = (input_height * input_width - input_height) + (input_height * input_width - input_width) + (input_height * input_width - input_height - input_width + 1);
        numSite = input_height * input_width;
        numLabel = 2;       

        unary_data = new int[numSite * numLabel];

        smooth = new int[numLabel * numLabel];
        for (int i = 0; i < numLabel; i++)
        {
            for (int j = 0; j < numLabel; j++)
            {
                if (i == j)
                    smooth[i + j * numLabel] = 0;
                else
                    smooth[i + j * numLabel] = (int)scale;
            }
        }

        cost = new double[numNeighbour, 3];

        //Initialize buffers
        posClick = new List<Vector2>();
        posMarker = new List<Vector2>();
        posWorld = new List<Vector3>();
        distance = new List<float>();
        textSet = new List<GameObject>();
        posInteraction = new List<List<Vector2>>();
        negInteraction = new List<List<Vector2>>();
        currentInteraction = new List<Vector2>();

        poly_min_quad = new Polygon();
        poly_contour = new Polygon();

        is_manual_bounding = false;
        //cnt_manual_bounding = 0;

        line_params = new List<List<double>>();

        idxSelSet_manual_bound = new List<int>();
    }

    // Update is called once per frame
    void Update()
    {
        /*
        h_start = (h_start + 10) % input_height;
        w_start = (w_start + 10) % input_width;
        color_rgb = Color.white;
        for (int h = 0; h < input_height; h++)
            for (int w = 0; w < input_width; w++)
            {
                view.SetPixel(w, h, color_rgb);
            }
        
        color_rgb = Color.red;
        for (int h = h_start; h < h_start + 80; h++)
            for (int w = w_start; w < w_start + 100; w++)
            {
                view.SetPixel(w, h, color_rgb);
            }

        img.texture = (Texture)view;
        view.Apply();
        */        
        if (is_register)
            ButtonRegister.colors = colors_button_on;
        else
            ButtonRegister.colors = colors_button_off;

        if(is_clear_view)
            ButtonClearView.colors = colors_button_on;
        else
            ButtonClearView.colors = colors_button_off;

        cam.update();
        get_depth();
        if (is_color)
        {
            if ((!cam.is_paused()))
                draw_color_view();
            else
            {
                if (is_clear_view)
                    if (!draw_seg)
                        draw_color_view();
                if (mode == 0)
                {
                    if (!is_clear_view)
                    {
                        for (int cnt = 0; cnt < posMarker.Count; cnt++)
                            draw_circle(posMarker[cnt]);
                        draw_line();
                    }
                }
                else if(mode==1)
                {
                    if (!is_clear_view)
                    {
                        ;
                        //draw_interaction();
                    }
                }
            }
        }
        else
        {
            if ((!cam.is_paused()))
                draw_depth_view();
            else
            {
                if (is_clear_view)
                    draw_depth_view();
                if (mode == 0)
                {
                    if (!is_clear_view)
                    {
                        for (int cnt = 0; cnt < posMarker.Count; cnt++)
                            draw_circle(posMarker[cnt]);
                        draw_line();
                    }
                }
            }
        }

        /*
        if(cam.is_paused())
        {
            if (mode == 0)
                draw_inpainted_depth();
        }
        */
    }

    private void coordinate_mapping_cam_to_unity(int x_cam, int y_cam, int input_height, ref int x_unity, ref int y_unity)
    {
        x_unity = x_cam;
        y_unity = input_height - y_cam;
    }

    public void on_mouse_up()
    {
        if (cam.is_paused() && mode == 0)
        {
            Vector2 point;
            RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform)img.rectTransform, Input.mousePosition, canvasCamera, out point);
            posClick.Add(new Vector2(point.x, point.y));
            posMarker.Add(new Vector2((point.x / view_width) * input_width, (point.y / view_height) * input_height));
            float worldX = 0;
            float worldY = 0;
            depth = (int)im_depth_inpaint.get((int)(input_height - posMarker[posMarker.Count - 1].y), (int)(posMarker[posMarker.Count - 1].x), 0, 5, 1);
            cam.coordinate_to_world((int)(posMarker[posMarker.Count - 1].x), (int)(input_height - posMarker[posMarker.Count - 1].y), depth, ref worldX, ref worldY);
            posWorld.Add(new Vector3(worldX, worldY, (float)depth));
            if (posWorld.Count >= 2 && posWorld.Count % 2 == 0)
            {
                float dist = Mathf.Sqrt((posWorld[posWorld.Count - 1].x - posWorld[posWorld.Count - 2].x) * (posWorld[posWorld.Count - 1].x - posWorld[posWorld.Count - 2].x) + (posWorld[posWorld.Count - 1].y - posWorld[posWorld.Count - 2].y) * (posWorld[posWorld.Count - 1].y - posWorld[posWorld.Count - 2].y) + (posWorld[posWorld.Count - 1].z - posWorld[posWorld.Count - 2].z) * (posWorld[posWorld.Count - 1].z - posWorld[posWorld.Count - 2].z));
                distance.Add(dist);
                //Debug.Log(dist); 


                double theta = Mathf.Atan2(posClick[posClick.Count - 1].y - posClick[posClick.Count - 2].y, posClick[posClick.Count - 1].x - posClick[posClick.Count - 2].x);

                double posX;
                double posY;
                if (posClick[posClick.Count - 1].x > posClick[posClick.Count - 2].x)
                {
                    posX = 0.5 * (posClick[posClick.Count - 1].x + posClick[posClick.Count - 2].x) - text_measure_shift * Mathf.Sin((float)theta);
                    posY = 0.5 * (posClick[posClick.Count - 1].y + posClick[posClick.Count - 2].y) + text_measure_shift * Mathf.Cos((float)theta);
                    double angle = theta * 180 / pi;
                    string text = dist.ToString("F1") + " mm";
                    add_text(new Vector2((float)posX, (float)posY), (float)angle, text);
                }
                else
                {
                    posX = 0.5 * (posClick[posClick.Count - 1].x + posClick[posClick.Count - 2].x) + text_measure_shift * Mathf.Sin((float)theta);
                    posY = 0.5 * (posClick[posClick.Count - 1].y + posClick[posClick.Count - 2].y) - text_measure_shift * Mathf.Cos((float)theta);
                    double angle = theta * 180 / pi;
                    string text = dist.ToString("F1") + " mm";
                    add_text(new Vector2((float)posX, (float)posY), (float)(angle + 180), text);
                }
            }
            /*
            Debug.Log(point.x);
            Debug.Log(point.y);
            Debug.Log("*************************");
            Debug.Log(posMarker[posMarker.Count-1].x);
            Debug.Log(posMarker[posMarker.Count-1].y);        
            Debug.Log("*************************");
            Debug.Log(posWorld[posWorld.Count - 1].x);
            Debug.Log(posWorld[posWorld.Count - 1].y);
            Debug.Log(posWorld[posWorld.Count - 1].z);
            Debug.Log("*************************");
            */
        }
        else if (cam.is_paused() && mode == 1 && is_manual_bounding == true)
        {
            Vector2 point;
            Vector2 p1 = new Vector2();
            Vector2 p2 = new Vector2();
            double px = 0, py = 0;

            RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform)img.rectTransform, Input.mousePosition, canvasCamera, out point);
            double x = (point.x / view_width) * input_width;
            double y = (point.y / view_height) * input_height;
            //draw_dashed_poly,draw_selected_boundary,cnt++ or cnt--            

            if (idxSelSet_manual_bound.Count < 4)
            {
                double minDist = 10000000;
                double tmpDist = 0;
                int idxMinDist = -1;
                for (int i = 0; i < line_params.Count; i++)
                {
                    if (((x - line_params[i][3]) * (x - line_params[i][5]) < 0) || ((y - line_params[i][4]) * (y - line_params[i][6]) < 0))
                    {
                        tmpDist = Mathf.Abs((float)(line_params[i][0] * x + line_params[i][1] * y + line_params[i][2])) / Mathf.Sqrt((float)(line_params[i][0] * line_params[i][0] + line_params[i][1] * line_params[i][1]));
                        if (tmpDist < minDist)
                        {
                            minDist = tmpDist;
                            idxMinDist = i;
                        }
                    }
                }

                if (idxMinDist != -1)
                {
                    //poly_contour.get_vertex(idxMinDist, ref px, ref py);
                    //py = input_height - py;
                    px = line_params[idxMinDist][3];
                    py = line_params[idxMinDist][4];
                    p1.x = (float)px;
                    p1.y = (float)py;

                    //poly_contour.get_vertex((idxMinDist + 1) % poly_contour.size(), ref px, ref py);
                    //py = input_height - py;
                    px = line_params[idxMinDist][5];
                    py = line_params[idxMinDist][6];
                    p2.x = (float)px;
                    p2.y = (float)py;

                    if (!idxSelSet_manual_bound.Contains(idxMinDist))
                    {
                        draw_solid_line(p1, p2, Color.green);
                        idxSelSet_manual_bound.Add(idxMinDist);
                    }
                    else
                    {
                        draw_solid_line(p1, p2, Color.red);
                        idxSelSet_manual_bound.Remove(idxMinDist);
                    }
                }
                Debug.Log(idxSelSet_manual_bound.Count);
                Debug.Log(poly_contour.size());
                Debug.Log(idxMinDist);
                Debug.Log(minDist);
            }
            if (idxSelSet_manual_bound.Count == 4)
            {
                Debug.Log("4 boundaries selected");
                idxSelSet_manual_bound.Sort();
                for (int i = 0; i < idxSelSet_manual_bound.Count; i++)
                {
                    int idx1 = idxSelSet_manual_bound[i];
                    int idx2 = idxSelSet_manual_bound[(i + 1) % idxSelSet_manual_bound.Count];
                    double x_intersect = 0, y_intersect = 0;
                    find_line_intersection(line_params[idx1][0], line_params[idx1][1], line_params[idx1][2], line_params[idx2][0], line_params[idx2][1], line_params[idx2][2], ref x_intersect, ref y_intersect);
                    poly_min_quad.add_vertex(x_intersect, input_height - y_intersect);
                }                
                draw_seg_rst(false);
                draw_poly(poly_min_quad, Color.green, false);
                draw_poly_based_measure(poly_min_quad, Color.green, false);
                is_manual_bounding = false;
            }            
        }
    }

    public void on_drag_begin()
    {
        currentInteraction = new List<Vector2>();
    }

    public void on_drag()
    {
        if(cam.is_paused() && mode ==1)
        {
            Vector2 point;
            RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform)img.rectTransform, Input.mousePosition, canvasCamera, out point);
            currentInteraction.Add(new Vector2((point.x / view_width) * input_width, (point.y / view_height) * input_height));
            if(currentInteraction.Count>=2)
            {
                if (is_positive_stroke)
                    draw_solid_line(currentInteraction[currentInteraction.Count - 2], currentInteraction[currentInteraction.Count - 1], Color.green);
                else
                    draw_solid_line(currentInteraction[currentInteraction.Count - 2], currentInteraction[currentInteraction.Count - 1], Color.red);
            }
        }
    }

    public void on_drag_end()
    {
        if(cam.is_paused() && mode ==1)
        {
            if (is_positive_stroke)
                posInteraction.Add(currentInteraction);
            else
                negInteraction.Add(currentInteraction);
        }
        update_click_pattern();        
        /*
        Debug.Log(currentInteraction.Count);
        Debug.Log(posInteraction.Count);
        Debug.Log(negInteraction.Count);
        Debug.Log(posInteraction[0].Count);
        Debug.Log(posInteraction[1].Count);
        Debug.Log("*******************");
        */
    }

    private void draw_interaction()
    {
        if(posInteraction.Count>0)
        { 
            for(int i=0;i<posInteraction.Count;i++)
            {
                if(posInteraction[i].Count>2)
                { 
                    for(int j=1;j< posInteraction[i].Count;j++)
                    {
                        draw_solid_line(posInteraction[i][j-1], posInteraction[i][j], Color.green);
                    }
                }
            }
        }

        if (negInteraction.Count > 0)
        {
            for (int i = 0; i < negInteraction.Count; i++)
            {
                if (negInteraction[i].Count > 2)
                {
                    for (int j = 1; j < negInteraction[i].Count; j++)
                    {
                        draw_solid_line(negInteraction[i][j - 1], negInteraction[i][j], Color.red);
                    }
                }
            }
        }
    }

    private void update_click_pattern()
    {
        im_pos_interaction_prev = im_pos_interaction.copy();
        im_neg_interaction_prev = im_neg_interaction.copy();

        cvMat tmpClickPattern = new cvMat();
        cvMat tmpX = new cvMat();
        cvMat tmpY = new cvMat();
        int step;
        if (currentInteraction.Count > 30)
            step = 2;
        else
            step = 1;

        if (is_positive_stroke)
        {
            for (int i = 0; i < currentInteraction.Count; i += step)
            {
                int x = (int)(currentInteraction[i].x);
                int y = (int)(input_height - currentInteraction[i].y);
                tmpX = meshX.subtract_scalar((double)x);
                tmpY = meshY.subtract_scalar((double)y);
                tmpClickPattern = (tmpX.multiply(tmpX).add(tmpY.multiply(tmpY))).sqrt();
                im_pos_interaction = im_pos_interaction.min(tmpClickPattern);
            }
        }
        else
        {
            for (int i = 0; i < currentInteraction.Count; i += step)
            {
                int x = (int)(currentInteraction[i].x);
                int y = (int)(input_height - currentInteraction[i].y);
                tmpX = meshX.subtract_scalar((double)x);
                tmpY = meshY.subtract_scalar((double)y);
                tmpClickPattern = (tmpX.multiply(tmpX).add(tmpY.multiply(tmpY))).sqrt();
                im_neg_interaction = im_neg_interaction.min(tmpClickPattern);
            }
        }
        //im_pos_interaction.convertTo(0).show(0);
        //im_neg_interaction.convertTo(0).show(1);
        tmpClickPattern.Dispose();
        tmpX.Dispose();
        tmpY.Dispose();
    }

    public void cancel_current_interaction()
    {
        if(mode==1&& currentInteraction.Count!=0)
        {
            //Debug.Log(posInteraction[0].Count);
            currentInteraction.Clear();
            //Debug.Log(posInteraction[0].Count);
            if (is_positive_stroke)
                posInteraction.RemoveAt(posInteraction.Count - 1);
            else
                negInteraction.RemoveAt(negInteraction.Count - 1);
            im_pos_interaction = im_pos_interaction_prev.copy();
            im_neg_interaction = im_neg_interaction_prev.copy();
            draw_color_view();
            draw_interaction();
        }
    }

    public void do_segmenation()
    {
        poly_min_quad.Dispose();
        poly_min_quad = new Polygon();
    
        if(textSet.Count>0)
            for (int i = textSet.Count - 1; i >= 0; i--)
            {
                Destroy(textSet[i]);
                textSet.RemoveAt(i);
            }
        //set input data
        net.bridge_mutable_input_data();
        double val = 0;
        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                val = im_bgr.get(h, w, 0, 16, 3) - mean_b;
                net.set_input_data(w + h * input_width, val);

                val = im_bgr.get(h, w, 1, 16, 3) - mean_g;
                net.set_input_data(w + h * input_width + input_height * input_width, val);

                val = im_bgr.get(h, w, 2, 16, 3) - mean_r;
                net.set_input_data(w + h * input_width + 2 * input_height * input_width, val);

                val = im_pos_interaction.get(h, w, 0, 6, 1) - mean_pos;
                net.set_input_data(w + h * input_width + 3 * input_height * input_width, val);

                val = im_neg_interaction.get(h, w, 0, 6, 1) - mean_neg;
                net.set_input_data(w + h * input_width + 4 * input_height * input_width, val);
            }
        }

        //forward
        net.forward();
        get_DNN_prob_map();

        //im_prob_map.convertTo(0).show(0);

        //graph cut
        calculate_unary_data();
        
        gCut.setDataCost(unary_data);
        
        Debug.Log("Energy before optimization is: " + gCut.compute_energy());
        gCut.expansion(5);
        Debug.Log("Energy after optimization is: " + gCut.compute_energy());
        
        int label = 0;

        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                label = (int)(255 * gCut.whatLabel(w + h * input_width));
                im_seg_rst.set(h, w, 0, 0, 1, (double)label);                
            }
        }

        //im_seg_rst.show(1);

        draw_seg_rst();

        draw_seg = true;

        Debug.Log("end of segmentation");
    }

    public void get_bounding_quad_auto()
    {
        if (mode == 1)
        {
            is_manual_bounding = false;

            draw_seg_rst(false);

            poly_min_quad.Dispose();
            poly_min_quad = new Polygon();

            if (textSet.Count > 0)
                for (int i = textSet.Count - 1; i >= 0; i--)
                {
                    Destroy(textSet[i]);
                    textSet.RemoveAt(i);
                }

            Polygon poly = im_seg_rst.getPolygon(width_morphology);
            poly_min_quad = poly.get_min_quad();
            draw_poly(poly_min_quad, Color.green, false);
            draw_poly_based_measure(poly_min_quad, Color.green, false);
            poly.Dispose();
        }
    }

    public void get_bounding_quad_manual()
    {
        if (mode == 1)
        {
            is_manual_bounding = true;

            draw_seg_rst(false);

            poly_min_quad.Dispose();
            poly_min_quad = new Polygon();

            if (textSet.Count > 0)
                for (int i = textSet.Count - 1; i >= 0; i--)
                {
                    Destroy(textSet[i]);
                    textSet.RemoveAt(i);
                }
            
            poly_contour = im_seg_rst.getPolygon(width_morphology);
            draw_poly(poly_contour, Color.red, true);

            line_params.Clear();            

            fit_lines(poly_contour, line_params);

            /*
            for (int i = 0; i < line_params.Count; i++)
            {
                Debug.Log(line_params[i][0] + ", " + line_params[i][1] + ", " + line_params[i][2]);                
            }
            */
        }
    }

    private void get_DNN_prob_map()
    {
        net.bridge_output_data();

        double val = 0;

        //get DNN output
        cvMat output_DNN_0 = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_1 = new cvMat(input_height, input_width, 5, 0);

        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                val = net.get_output_data(w + h * input_width);
                output_DNN_0.set(h, w, 0, 5, 1, val);

                val = net.get_output_data(w + h * input_width + input_height * input_width);
                output_DNN_1.set(h, w, 0, 5, 1, val);
            }
        }

        //calculate prob map
        double min = 0;
        double max = 0;
        cvMat output_DNN_0_neg = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_1_neg = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_0_exp = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_1_exp = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_0_exp_p1 = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_1_exp_p1 = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_0_exp_p1_d1 = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_1_exp_p1_d1 = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_sum = new cvMat(input_height, input_width, 5, 0);
        cvMat output_DNN_prob = new cvMat(input_height, input_width, 5, 0);        

        output_DNN_0_neg = output_DNN_0.multiply_scalar((double)(-1.0));
        output_DNN_1_neg = output_DNN_1.multiply_scalar((double)(-1.0));

        output_DNN_0_exp = output_DNN_0_neg.exp();
        output_DNN_1_exp = output_DNN_1_neg.exp();

        output_DNN_0_exp_p1 = output_DNN_0_exp.add_scalar((double)1.0);
        output_DNN_1_exp_p1 = output_DNN_1_exp.add_scalar((double)1.0);

        output_DNN_0_exp_p1_d1 = output_DNN_0_exp_p1.divide_scalar((double)1.0);
        output_DNN_1_exp_p1_d1 = output_DNN_1_exp_p1.divide_scalar((double)1.0);

        output_DNN_sum = output_DNN_0_exp_p1_d1.add(output_DNN_1_exp_p1_d1);

        output_DNN_prob = output_DNN_1_exp_p1_d1.divide(output_DNN_sum);

        output_DNN_prob.minMax(ref min, ref max);

        im_prob_map = output_DNN_prob.normalize(min, max, (double)255);

        im_prob_map.minMax(ref min, ref max);

        //replace the min and max value to be out of bound
        cvMat mask_0 = new cvMat();
        cvMat mask_255 = new cvMat();
        cvMat mask_n0 = new cvMat();
        cvMat mask_n255 = new cvMat();
        cvMat mask_all = new cvMat();

        mask_n0 = im_prob_map.isNotEqual(0);
        mask_n255 = im_prob_map.isNotEqual(255);
        mask_0 = im_prob_map.isEqual(0);
        mask_255 = im_prob_map.isEqual(255);
        mask_all = mask_n0.bitwise_and(mask_n255);
        im_prob_map.minMax_mask(ref min, ref max, mask_all);

        //Debug.Log("****");
        //Debug.Log("Min: " + min + "; Max: " + max);

        im_prob_map.setTo_mask((double)min, mask_0);
        im_prob_map.setTo_mask((double)max, mask_255);

        //im_prob_map.minMax(ref min, ref max);

        //Debug.Log("****");
        //Debug.Log("Min: " + min + "; Max: " + max);        

        mask_0.Dispose();
        mask_255.Dispose();
        mask_n0.Dispose();
        mask_n255.Dispose();
        mask_all.Dispose();

        output_DNN_0_neg.Dispose();
        output_DNN_1_neg.Dispose();
        output_DNN_0_exp.Dispose();
        output_DNN_1_exp.Dispose();
        output_DNN_0_exp_p1.Dispose();
        output_DNN_1_exp_p1.Dispose();
        output_DNN_0_exp_p1_d1.Dispose();
        output_DNN_1_exp_p1_d1.Dispose();
        output_DNN_sum.Dispose();
        output_DNN_prob.Dispose();
    }

    private void calculate_unary_data()
    {
        cvMat posInteractionMask = new cvMat();
        cvMat negInteractionMask = new cvMat();

        posInteractionMask = im_pos_interaction.isLess((double)stroke_effect_radius);
        negInteractionMask = im_neg_interaction.isLess((double)stroke_effect_radius);

        //Debug.Log(posInteractionMask.type());
        //Debug.Log(negInteractionMask.type());

        double valMin = 0;
        double valMax = 0;
        double maxDiff = 0;

        //posInteractionMask.minMax(ref valMin, ref valMax);
        //Debug.Log(valMin + ", " + valMax);
        //negInteractionMask.minMax(ref valMin, ref valMax);
        //Debug.Log(valMin + ", " + valMax);

        im_prob_map.minMax(ref valMin, ref valMax);

        valMin = valMin / 255.0;
        valMax = valMax / 255.0;

        if ((1 - valMin) > valMax)
            maxDiff = Mathf.Abs((-1.0F) * Mathf.Log((float)(1 - valMin)) - (-1.0F) * Mathf.Log((float)valMin));
        else
            maxDiff = Mathf.Abs((-1.0F) * Mathf.Log((float)(1 - valMax)) - (-1.0F) * Mathf.Log((float)valMax));

        float prob = 0;
        bool isPos = false;
        bool isNeg = false;

        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                prob = (float)(im_prob_map.get(h, w, 0, 5, 1) / 255.0);
                isPos = System.Convert.ToBoolean(posInteractionMask.get(h, w, 0, 0, 1));
                isNeg = System.Convert.ToBoolean(negInteractionMask.get(h, w, 0, 0, 1));
                if (isPos)
                {
                    unary_data[numLabel * (w + h * input_width)] = (int)(scale * lambda * ((-1) * Mathf.Log(1 - prob) + (-1) * Mathf.Log(prob) + maxDiff));
                    unary_data[numLabel * (w + h * input_width) + 1] = (int)(scale * lambda * ((-1) * Mathf.Log(1 - prob) + (-1) * Mathf.Log(prob)));
                }
                else if (isNeg)
                {
                    unary_data[numLabel * (w + h * input_width)] = (int)(scale * lambda * ((-1) * Mathf.Log(1 - prob) + (-1) * Mathf.Log(prob)));
                    unary_data[numLabel * (w + h * input_width) + 1] = (int)(scale * lambda * ((-1) * Mathf.Log(1 - prob) + (-1) * Mathf.Log(prob) + maxDiff));
                }
                else
                {
                    unary_data[numLabel * (w + h * input_width)] = (int)((-1) * scale * lambda * Mathf.Log(1 - prob));
                    unary_data[numLabel * (w + h * input_width) + 1] = (int)((-1) * scale * lambda * Mathf.Log(prob));
                }
            }
        }
    }

    private void calculate_neighbour()
    {
        //transfer to LAB color space
        cvMat img_LAB = new cvMat();
        cvMat img_LAB_FC = new cvMat();
        cvMat img_L = new cvMat();
        cvMat img_A = new cvMat();
        cvMat img_B = new cvMat();
        cvMat img_L_ori = new cvMat();
        cvMat img_A_ori = new cvMat();
        cvMat img_B_ori = new cvMat();
        img_LAB = im_bgr.cvtColor_BGR2LAB();
        img_LAB_FC = img_LAB.convertTo(21);

        img_L = img_LAB_FC.extractChannel(0);
        img_A = img_LAB_FC.extractChannel(1);
        img_B = img_LAB_FC.extractChannel(2);

        img_L_ori = img_L.multiply_scalar((double)(100.0 / 255.0));
        img_A_ori = img_A.subtract_scalar((double)128.0);
        img_B_ori = img_B.subtract_scalar((double)128.0);

        int cnt = 0;

        //horintal neighbour
        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width - 1; w++)
            {
                float L1 = (float)img_L_ori.get(h, w, 0, 5, 1);
                float L2 = (float)img_L_ori.get(h, w + 1, 0, 5, 1);
                float A1 = (float)img_A_ori.get(h, w, 0, 5, 1);
                float A2 = (float)img_A_ori.get(h, w + 1, 0, 5, 1);
                float B1 = (float)img_B_ori.get(h, w, 0, 5, 1);
                float B2 = (float)img_B_ori.get(h, w + 1, 0, 5, 1);
                float tmpDist = (L1 - L2) * (L1 - L2) + (A1 - A2) * (A1 - A2) + (B1 - B2) * (B1 - B2);
                cost[cnt, 0] = w + h * input_width;
                cost[cnt, 1] = w + h * input_width + 1;
                cost[cnt, 2] = scale * Mathf.Exp((-tmpDist) / (2 * sigma * sigma));
                cnt++;
            }
        }

        //vertical neighbour
        for (int h = 0; h < input_height - 1; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                float L1 = (float)img_L_ori.get(h, w, 0, 5, 1);
                float L2 = (float)img_L_ori.get(h + 1, w, 0, 5, 1);
                float A1 = (float)img_A_ori.get(h, w, 0, 5, 1);
                float A2 = (float)img_A_ori.get(h + 1, w, 0, 5, 1);
                float B1 = (float)img_B_ori.get(h, w, 0, 5, 1);
                float B2 = (float)img_B_ori.get(h + 1, w, 0, 5, 1);
                float tmpDist = (L1 - L2) * (L1 - L2) + (A1 - A2) * (A1 - A2) + (B1 - B2) * (B1 - B2);
                cost[cnt, 0] = w + h * input_width;
                cost[cnt, 1] = w + h * input_width + input_width;
                cost[cnt, 2] = scale * Mathf.Exp((-tmpDist) / (2 * sigma * sigma));
                cnt++;
            }
        }

        //diagonal neighbour
        for (int h = 0; h < input_height - 1; h++)
        {
            for (int w = 0; w < input_width - 1; w++)
            {
                float L1 = (float)img_L_ori.get(h, w, 0, 5, 1);
                float L2 = (float)img_L_ori.get(h + 1, w + 1, 0, 5, 1);
                float A1 = (float)img_A_ori.get(h, w, 0, 5, 1);
                float A2 = (float)img_A_ori.get(h + 1, w + 1, 0, 5, 1);
                float B1 = (float)img_B_ori.get(h, w, 0, 5, 1);
                float B2 = (float)img_B_ori.get(h + 1, w + 1, 0, 5, 1);
                float tmpDist = (L1 - L2) * (L1 - L2) + (A1 - A2) * (A1 - A2) + (B1 - B2) * (B1 - B2);
                cost[cnt, 0] = w + h * input_width;
                cost[cnt, 1] = w + h * input_width + input_width + 1;
                cost[cnt, 2] = scale * Mathf.Exp((-tmpDist) / (2 * sigma * sigma)) * (1.0 / Mathf.Sqrt((float)2.0));
                cnt++;
            }
        }

        Debug.Log(cnt +  " / " + numNeighbour + " neighbours set");

        img_LAB.Dispose();
        img_LAB_FC.Dispose();
        img_L.Dispose();
        img_A.Dispose();
        img_B.Dispose();
        img_L_ori.Dispose();
        img_A_ori.Dispose();
        img_B_ori.Dispose();
    }

    private void add_text(Vector2 pos, float angle, string content)
    {
        GameObject UItextGO = new GameObject("TextMeasure");
        UItextGO.transform.SetParent(ImageOnPanel.transform);

        RectTransform trans = UItextGO.AddComponent<RectTransform>();
        trans.anchorMin = new Vector2(0, 0);
        trans.anchorMax = new Vector2(0, 0);

        trans.anchoredPosition = pos;
        trans.localPosition = new Vector3(trans.localPosition.x, trans.localPosition.y, 0);
        trans.localScale = new Vector3(1, 1, 1);
        trans.sizeDelta = new Vector2(600, 100);
        trans.Rotate(new Vector3(0, 0, angle));

        Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
        Text text = UItextGO.AddComponent<Text>();
        text.font = ArialFont;
        text.fontSize = 40;
        text.fontStyle = FontStyle.Bold;
        text.color = new Color(1.0F, 1.0F, 0.0F);
        text.alignment = TextAnchor.MiddleCenter;

        text.text = content;
        textSet.Add(UItextGO);
    }

    private void add_text(Vector2 pos, float angle, string content, Color c)
    {
        GameObject UItextGO = new GameObject("TextMeasure");
        UItextGO.transform.SetParent(ImageOnPanel.transform);

        RectTransform trans = UItextGO.AddComponent<RectTransform>();
        trans.anchorMin = new Vector2(0, 0);
        trans.anchorMax = new Vector2(0, 0);

        trans.anchoredPosition = pos;
        trans.localPosition = new Vector3(trans.localPosition.x, trans.localPosition.y, 0);
        trans.localScale = new Vector3(1, 1, 1);
        trans.sizeDelta = new Vector2(600, 100);
        trans.Rotate(new Vector3(0, 0, angle));

        Font ArialFont = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
        Text text = UItextGO.AddComponent<Text>();
        text.font = ArialFont;
        text.fontSize = 40;
        text.fontStyle = FontStyle.Bold;
        text.color = c;
        text.alignment = TextAnchor.MiddleCenter;

        text.text = content;
        textSet.Add(UItextGO);
    }

    private void draw_color_view()
    {
        //get rgb texture
        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                cam.get_color_data(w + h * input_width, ref r, ref g, ref b);

                im_bgr.set(h, w, 0, 16, 3, (double)b);
                im_bgr.set(h, w, 1, 16, 3, (double)g);
                im_bgr.set(h, w, 2, 16, 3, (double)r);

                color_rgb = new Color((float)(r / 255.0), (float)(g / 255.0), (float)(b / 255.0));
                coordinate_mapping_cam_to_unity(w, h, input_height, ref x_unity, ref y_unity);
                view.SetPixel(x_unity, y_unity, color_rgb);
            }
        }
        img.texture = (Texture)view;
        view.Apply();
    }
    private void get_depth()
    {
        //get depth texture
        im_depth_register.Dispose();
        im_depth_register = new cvMat(input_height, input_width, 5, 0);
        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                depth = cam.get_depth_data(w + h * input_width);
                im_depth.set(h, w, 0, 5, 1, (double)depth);

                cam.coordinate_depth_to_color_v2(w, h, depth, ref x_register, ref y_register);

                if (y_register >= 0 && y_register < input_height && x_register >= 0 && x_register < input_width)
                    im_depth_register.set(y_register, x_register, 0, 5, 1, (double)depth);
            }
        }
        //print(y_register + ", " + x_register);          
    }

    private void draw_depth_view()
    {           
        //for unregistered depth
        if (!is_register)
        {
            im_depth.minMax(ref minDepth, ref maxDepth);
            //print(minDepth + ", " + maxDepth);
            im_depth_normalize = im_depth.normalize(minDepth, maxDepth, (double)1.0);
            for (int h = 0; h < input_height; h++)
            {
                for (int w = 0; w < input_width; w++)
                {
                    depth_normalize = (float)im_depth_normalize.get(h, w, 0, 5, 1);

                    color_rgb = new Color(depth_normalize, depth_normalize, depth_normalize);

                    coordinate_mapping_cam_to_unity(w, h, input_height, ref x_unity, ref y_unity);
                    view.SetPixel(x_unity, y_unity, color_rgb);
                }
            }
        }
        else //for registered depth
        {
            im_depth_register.minMax(ref minDepth, ref maxDepth);
            //print(minDepth + ", " + maxDepth);
            im_depth_normalize = im_depth_register.normalize(minDepth, maxDepth, (double)1.0);
            for (int h = 0; h < input_height; h++)
            {
                for (int w = 0; w < input_width; w++)
                {
                    depth_normalize = (float)im_depth_normalize.get(h, w, 0, 5, 1);

                    color_rgb = new Color(depth_normalize, depth_normalize, depth_normalize);

                    coordinate_mapping_cam_to_unity(w, h, input_height, ref x_unity, ref y_unity);
                    view.SetPixel(x_unity, y_unity, color_rgb);
                }
            }
        }
        img.texture = (Texture)view;
        view.Apply();
    }

    private void draw_circle(Vector2 cen)
    {
        float x = 0;
        float y = 0;
        color_rgb = new Color(1.0F, 1.0F, 0.0F);
        
        for (float theta=0;theta<2*pi; theta += 0.1F)
            for(float rad=circle_inner_radius; rad<=circle_outer_radius;rad+=0.1F)
            {
                x = cen.x + rad * Mathf.Cos(theta);
                y = cen.y + rad * Mathf.Sin(theta);

                view.SetPixel((int)x, (int)y, color_rgb);
            }        
        img.texture = (Texture)view;
        view.Apply();
    }

    private void draw_circle_color(Vector2 cen, Color c)
    {
        float x = 0;
        float y = 0;        

        for (float theta = 0; theta < 2 * pi; theta += 0.1F)
            for (float rad = circle_inner_radius; rad <= circle_outer_radius; rad += 0.1F)
            {
                x = cen.x + rad * Mathf.Cos(theta);
                y = cen.y + rad * Mathf.Sin(theta);

                view.SetPixel((int)x, (int)y, c);
            }
        img.texture = (Texture)view;
        view.Apply();
    }

    private void draw_line()
    {
        color_rgb = new Color(1.0F, 1.0F, 0.0F);
        
        if (posMarker.Count>=2)
        {
            for(int i=0;i+1<posMarker.Count;i+=2)
            {
                float dist = Mathf.Sqrt((posMarker[i].x - posMarker[i + 1].x) * (posMarker[i].x - posMarker[i + 1].x) + (posMarker[i].y - posMarker[i + 1].y) * (posMarker[i].y - posMarker[i + 1].y));
                float theta = Mathf.Atan2(posMarker[i + 1].y - posMarker[i].y, posMarker[i + 1].x - posMarker[i].x);
                int dx = 0;
                int dy = 0;
                int cnt_dot_line_len = 0;
                for(int len=circle_outer_radius;len<=dist-circle_inner_radius;len++)
                {
                    if(cnt_dot_line_len < dot_line_length)                    
                        for (float w=(-line_width/2.0F); w <= (line_width / 2.0F);w+=0.1F)
                        {                    
                            dx = (int)(posMarker[i].x + len * Mathf.Cos(theta) + w * Mathf.Sin(theta));
                            dy = (int)(posMarker[i].y + len * Mathf.Sin(theta) - w * Mathf.Cos(theta));
                            view.SetPixel(dx, dy, color_rgb);                    
                        }
                    cnt_dot_line_len = (cnt_dot_line_len + 1) % (2 * dot_line_length);
                }
            }
        }
        
        img.texture = (Texture)view;
        view.Apply();
    }

    private void draw_dashed_line(Vector2 p1, Vector2 p2, Color c)
    {
        float dist = Mathf.Sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
        float theta = Mathf.Atan2(p2.y - p1.y, p2.x - p1.x);
        int dx = 0;
        int dy = 0;
        int cnt_dot_line_len = 0;
        for (int len = 0; len <= dist; len++)
        {
            if (cnt_dot_line_len < dot_line_length)
                for (float w = (-stroke_width / 2.0F); w <= (stroke_width / 2.0F); w += 0.1F)
                {
                    dx = (int)(p1.x + len * Mathf.Cos(theta) + w * Mathf.Sin(theta));
                    dy = (int)(p1.y + len * Mathf.Sin(theta) - w * Mathf.Cos(theta));
                    view.SetPixel(dx, dy, c);
                }
            cnt_dot_line_len = (cnt_dot_line_len + 1) % (2 * dot_line_length);
        }
        img.texture = (Texture)view;
        view.Apply();
    }

    private void draw_solid_line(Vector2 p1, Vector2 p2, Color c)
    {
        float dist = Mathf.Sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
        float theta = Mathf.Atan2(p2.y - p1.y, p2.x - p1.x);
        int dx = 0;
        int dy = 0;
        for (int len = 0; len <= dist; len++)
        {
            for (float w = (-stroke_width / 2.0F); w <= (stroke_width / 2.0F); w += 0.1F)
            {
                dx = (int)(p1.x + len * Mathf.Cos(theta) + w * Mathf.Sin(theta));
                dy = (int)(p1.y + len * Mathf.Sin(theta) - w * Mathf.Cos(theta));
                view.SetPixel(dx, dy, c);
            }
        }
        img.texture = (Texture)view;
        view.Apply();
    }

    private void draw_seg_rst()
    {
        double px_b;
        double px_g;
        double px_r;        
        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                px_b = color_weight_overlay * im_bgr.get(h, w, 0, 16, 3);
                px_g = color_weight_overlay * im_bgr.get(h, w, 1, 16, 3) + (1 - color_weight_overlay) * im_seg_rst.get(h, w, 0, 0, 1);
                px_r = color_weight_overlay * im_bgr.get(h, w, 2, 16, 3) + (1 - color_weight_overlay) * im_seg_rst.get(h, w, 0, 0, 1);

                color_rgb = new Color((float)(px_r / 255.0), (float)(px_g / 255.0), (float)(px_b / 255.0));
                coordinate_mapping_cam_to_unity(w, h, input_height, ref x_unity, ref y_unity);
                view.SetPixel(x_unity, y_unity, color_rgb);
            }
        }
        img.texture = (Texture)view;
        view.Apply();
        draw_interaction();
    }

    private void draw_seg_rst(bool is_draw_interaction)
    {
        double px_b;
        double px_g;
        double px_r;
        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                px_b = color_weight_overlay * im_bgr.get(h, w, 0, 16, 3);
                px_g = color_weight_overlay * im_bgr.get(h, w, 1, 16, 3) + (1 - color_weight_overlay) * im_seg_rst.get(h, w, 0, 0, 1);
                px_r = color_weight_overlay * im_bgr.get(h, w, 2, 16, 3) + (1 - color_weight_overlay) * im_seg_rst.get(h, w, 0, 0, 1);

                color_rgb = new Color((float)(px_r / 255.0), (float)(px_g / 255.0), (float)(px_b / 255.0));
                coordinate_mapping_cam_to_unity(w, h, input_height, ref x_unity, ref y_unity);
                view.SetPixel(x_unity, y_unity, color_rgb);
            }
        }
        img.texture = (Texture)view;
        view.Apply();
        if (is_draw_interaction)
            draw_interaction();
    }

    private void inpaint_depth()
    {
        cvMat tmpMat = new cvMat();
        cvMat mask = new cvMat();
        cvMat tmpImDepthInpaint = new cvMat();
        cvMat tmpImDepthInpaintNormalize = new cvMat();

        im_depth_inpaint = im_depth_register.copy();
        im_depth_inpaint.minMax(ref minDepth, ref maxDepth);
        tmpImDepthInpaintNormalize = im_depth_inpaint.normalize(minDepth, maxDepth, (double)255.0);

        tmpMat = tmpImDepthInpaintNormalize.convertTo(0);
        mask = tmpMat.isEqual(0);        
        tmpImDepthInpaint = tmpMat.inpaint(mask, 3, 1);
        double d = 0;
        double d_inpaint = 0;
        for (int h = 0; h < input_height; h++)
            for (int w = 0; w < input_width; w++)
            {
                d = im_depth_inpaint.get(h, w, 0, 5, 1);
                if (d == 0)
                {
                    d_inpaint = tmpImDepthInpaint.get(h, w, 0, 0, 1) * (maxDepth - minDepth) / 255.0 + minDepth;
                    im_depth_inpaint.set(h, w, 0, 5, 1, d_inpaint);
                }
            }

        tmpMat.Dispose();
        mask.Dispose();
        tmpImDepthInpaint.Dispose();
    }

    private void draw_inpainted_depth()
    {
        im_depth_inpaint.minMax(ref minDepth, ref maxDepth);
        im_depth_inpaint_normalize = im_depth_inpaint.normalize(minDepth, maxDepth, 1.0);
        for (int h = 0; h < input_height; h++)
        {
            for (int w = 0; w < input_width; w++)
            {
                depth_normalize = (float)im_depth_inpaint_normalize.get(h, w, 0, 5, 1);

                color_rgb = new Color(depth_normalize, depth_normalize, depth_normalize);

                coordinate_mapping_cam_to_unity(w, h, input_height, ref x_unity, ref y_unity);
                view_rst.SetPixel(x_unity, y_unity, color_rgb);
            }
        }
            
        img_rst.texture = (Texture)view_rst;
        view_rst.Apply();
    }

    private void draw_poly(Polygon poly, Color c, bool is_use_marker)
    {
        double x = 0;
        double y = 0;
        double x_next = 0;
        double y_next = 0;
        for (int i = 0; i < poly.size(); i++)
        {
            int i_next = (i + 1) % poly.size();
            poly.get_vertex(i, ref x, ref y);
            poly.get_vertex(i_next, ref x_next, ref y_next);

            if (is_use_marker)
                draw_circle_color(new Vector2((float)x, (float)(input_height - y)),c);
            draw_solid_line(new Vector2((float)x, (float)(input_height - y)), new Vector2((float)x_next, (float)(input_height - y_next)), c);
        }
    }

    private void draw_poly_dashed_line(Polygon poly, Color c, bool is_use_marker)
    {
        double x = 0;
        double y = 0;
        double x_next = 0;
        double y_next = 0;
        for (int i = 0; i < poly.size(); i++)
        {
            int i_next = (i + 1) % poly.size();
            poly.get_vertex(i, ref x, ref y);
            poly.get_vertex(i_next, ref x_next, ref y_next);

            if (is_use_marker)
                draw_circle_color(new Vector2((float)x, (float)(input_height - y)), c);
            draw_dashed_line(new Vector2((float)x, (float)(input_height - y)), new Vector2((float)x_next, (float)(input_height - y_next)), c);
        }
    }

    private void draw_poly_based_measure(Polygon poly, Color c)
    {
        double x = 0;
        double y = 0;
        double x_next = 0;
        double y_next = 0;
        float theta;
        float theta_2pi;
        double posX = 0;
        double posY = 0;

        Vector3 pos_world;
        Vector3 pos_world_next;

        float worldX = 0;
        float worldY = 0;

        for (int i = 0; i < poly.size(); i++)
        {
            int i_next = (i + 1) % poly.size();
            poly.get_vertex(i, ref x, ref y);
            poly.get_vertex(i_next, ref x_next, ref y_next);

            y = input_height - y;
            y_next = input_height - y_next;

            theta = Mathf.Atan2((float)(y_next - y), (float)(x_next - x));

            theta_2pi = theta + (float) (pi / 2);

            draw_solid_line(new Vector2((float)x, (float)y), new Vector2((float)(x+ height_measure_quad*Mathf.Cos(theta_2pi)), (float)(y + height_measure_quad * Mathf.Sin(theta_2pi))), Color.red);

            draw_solid_line(new Vector2((float)x_next, (float)y_next), new Vector2((float)(x_next + height_measure_quad * Mathf.Cos(theta_2pi)), (float)(y_next + height_measure_quad * Mathf.Sin(theta_2pi))), Color.red);

            draw_dashed_line(new Vector2((float)(x + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi)), (float)(y + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi))), new Vector2((float)(x_next + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi)), (float)(y_next + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi))), Color.red);

            
            depth = (int)im_depth_inpaint.get((int)(input_height - y), (int)(x), 0, 5, 1);
            cam.coordinate_to_world((int)(x), (int)(input_height - y), depth, ref worldX, ref worldY);
            pos_world = new Vector3(worldX, worldY, (float)depth);

            depth = (int)im_depth_inpaint.get((int)(input_height - y_next), (int)(x_next), 0, 5, 1);
            cam.coordinate_to_world((int)(x_next), (int)(input_height - y_next), depth, ref worldX, ref worldY);
            pos_world_next = new Vector3(worldX, worldY, (float)depth);

            float dist = Mathf.Sqrt((pos_world.x - pos_world_next.x) * (pos_world.x - pos_world_next.x) + (pos_world.y - pos_world_next.y) * (pos_world.y - pos_world_next.y) + (pos_world.z - pos_world_next.z) * (pos_world.z - pos_world_next.z));
            
            if (x_next > x)
            {
                posX = 0.5 * (((x * view_width) / input_width + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi)) + ((x_next * view_width) / input_width + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi))) - text_measure_shift * Mathf.Sin((float)theta);
                posY = 0.5 * (((y * view_height) / input_height + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi)) + ((y_next * view_height) / input_height + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi))) + text_measure_shift * Mathf.Cos((float)theta);
                double angle = theta * 180 / pi;
                string text = dist.ToString("F1") + " mm";
                add_text(new Vector2((float)posX, (float)posY), (float)angle, text, Color.red);
            }
            else
            {
                posX = 0.5 * (((x * view_width) / input_width + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi)) + ((x_next * view_width) / input_width + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi))) + text_measure_shift * Mathf.Sin((float)theta);
                posY = 0.5 * (((y * view_height) / input_height + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi)) + ((y_next * view_height) / input_height + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi))) - text_measure_shift * Mathf.Cos((float)theta);
                double angle = theta * 180 / pi;
                string text = dist.ToString("F1") + " mm";
                add_text(new Vector2((float)posX, (float)posY), (float)(angle + 180), text, Color.red);
            }
            
            //Debug.Log(theta);
        }
    }

    private void draw_poly_based_measure(Polygon poly, Color c, bool is_auxiliary_line)
    {
        double x = 0;
        double y = 0;
        double x_next = 0;
        double y_next = 0;
        float theta;
        float theta_2pi;
        double posX = 0;
        double posY = 0;

        Vector3 pos_world;
        Vector3 pos_world_next;

        float worldX = 0;
        float worldY = 0;

        for (int i = 0; i < poly.size(); i++)
        {
            int i_next = (i + 1) % poly.size();
            poly.get_vertex(i, ref x, ref y);
            poly.get_vertex(i_next, ref x_next, ref y_next);

            y = input_height - y;
            y_next = input_height - y_next;

            theta = Mathf.Atan2((float)(y_next - y), (float)(x_next - x));

            theta_2pi = theta + (float)(pi / 2);

            if (is_auxiliary_line)
            {
                draw_solid_line(new Vector2((float)x, (float)y), new Vector2((float)(x + height_measure_quad * Mathf.Cos(theta_2pi)), (float)(y + height_measure_quad * Mathf.Sin(theta_2pi))), Color.red);

                draw_solid_line(new Vector2((float)x_next, (float)y_next), new Vector2((float)(x_next + height_measure_quad * Mathf.Cos(theta_2pi)), (float)(y_next + height_measure_quad * Mathf.Sin(theta_2pi))), Color.red);

                draw_dashed_line(new Vector2((float)(x + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi)), (float)(y + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi))), new Vector2((float)(x_next + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi)), (float)(y_next + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi))), Color.red);
            }


            depth = (int)im_depth_inpaint.get((int)(input_height - y), (int)(x), 0, 5, 1);
            cam.coordinate_to_world((int)(x), (int)(input_height - y), depth, ref worldX, ref worldY);
            pos_world = new Vector3(worldX, worldY, (float)depth);

            depth = (int)im_depth_inpaint.get((int)(input_height - y_next), (int)(x_next), 0, 5, 1);
            cam.coordinate_to_world((int)(x_next), (int)(input_height - y_next), depth, ref worldX, ref worldY);
            pos_world_next = new Vector3(worldX, worldY, (float)depth);

            float dist = Mathf.Sqrt((pos_world.x - pos_world_next.x) * (pos_world.x - pos_world_next.x) + (pos_world.y - pos_world_next.y) * (pos_world.y - pos_world_next.y) + (pos_world.z - pos_world_next.z) * (pos_world.z - pos_world_next.z));

            if (x_next > x)
            {
                posX = 0.5 * (((x * view_width) / input_width + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi)) + ((x_next * view_width) / input_width + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi))) - text_measure_shift * Mathf.Sin((float)theta);
                posY = 0.5 * (((y * view_height) / input_height + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi)) + ((y_next * view_height) / input_height + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi))) + text_measure_shift * Mathf.Cos((float)theta);
                double angle = theta * 180 / pi;
                string text = dist.ToString("F1") + " mm";
                add_text(new Vector2((float)posX, (float)posY), (float)angle, text, Color.red);
            }
            else
            {
                posX = 0.5 * (((x * view_width) / input_width + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi)) + ((x_next * view_width) / input_width + (0.5 * height_measure_quad) * Mathf.Cos(theta_2pi))) + text_measure_shift * Mathf.Sin((float)theta);
                posY = 0.5 * (((y * view_height) / input_height + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi)) + ((y_next * view_height) / input_height + (0.5 * height_measure_quad) * Mathf.Sin(theta_2pi))) - text_measure_shift * Mathf.Cos((float)theta);
                double angle = theta * 180 / pi;
                string text = dist.ToString("F1") + " mm";
                add_text(new Vector2((float)posX, (float)posY), (float)(angle + 180), text, Color.red);
            }

            //Debug.Log(theta);
        }
    }

    private void fit_lines(Polygon polygon, List<List<double>> line_params)
    {
        int i_next = 0;
        double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
        for (int i = 0; i < polygon.size(); i++)
        {
            i_next = (i + 1) % polygon.size();

            polygon.get_vertex(i, ref x1, ref y1);
            y1 = input_height - y1;

            polygon.get_vertex(i_next, ref x2, ref y2);
            y2 = input_height - y2;

            List<double> param = new List<double>();
            param.Add(y1 - y2);
            param.Add(x2 - x1);
            param.Add((x1 - x2) * y1 + (y2 - y1) * x1);
            param.Add(x1);
            param.Add(y1);
            param.Add(x2);
            param.Add(y2);
            line_params.Add(param);
        }
    }

    private bool find_line_intersection(double a1, double b1, double c1, double a2, double b2, double c2, ref double x, ref double y)
    {
        if (a1 * b2 == a2 * b1)
            return false;

        x = (c2 * b1 - c1 * b2) / (a1 * b2 - a2 * b1);

        y = (c2 * a1 - c1 * a2) / (b1 * a2 - b2 * a1);

        return true;
        
    }

    private void update_view()
    {
        if (is_color)
            draw_color_view();
        else
            draw_depth_view();
    }

    public void toggle_pause()
    {
        cam.toggle_pause();
        if (cam.is_paused())
        {
            inpaint_depth();
            draw_inpainted_depth();
            calculate_neighbour();
            ButtonChangeMode.enabled = true; //enable model selection when the camera is paused;
            gCut = new GraphCutOptimizationGeneralGraph(numSite, numLabel);
            gCut.setSmoothCost(smooth);

            for (int i = 0; i < numNeighbour; i++)
                gCut.setNeighbors((int)cost[i, 0], (int)cost[i, 1], (int)cost[i, 2]);
        }
        else
        {
            ButtonChangeMode.enabled = false;
            gCut.Dispose();
            posInteraction.Clear();
            negInteraction.Clear();
            currentInteraction.Clear();
            im_pos_interaction.Dispose();
            im_neg_interaction.Dispose();
            im_prob_map.Dispose();
            im_pos_interaction = (new cvMat(input_height, input_width, 6, 1)).multiply_scalar(255.0);
            im_neg_interaction = (new cvMat(input_height, input_width, 6, 1)).multiply_scalar(255.0);
            im_prob_map = new cvMat(input_height, input_width, 5, 0);
        }        
    }

    public void toggle_color_depth()
    {
        is_color = !is_color;
        for (int i = 0; i < textSet.Count; i++)
            textSet[i].GetComponent<Text>().enabled = false;
        if (cam.is_paused())
            update_view();
    }

    public void toggle_register()
    {
        is_register = !is_register;
    }

    public void change_mode()
    {        
        mode = (mode + 1) % 2;
        if (mode == 0)
        { 
            ButtonChangeMode.GetComponentInChildren<Text>().text = "Mode: Measure";
            ButtonStrokeType.SetActive(false);
            ButtonCaffeMode.SetActive(false);
            ButtonDoSegmentation.SetActive(false);
            ButtonCancelCurrentStroke.SetActive(false);
            ButtonAutoBound.SetActive(false);
            ButtonManualBound.SetActive(false);
        }
        else if(mode == 1)
        { 
            ButtonChangeMode.GetComponentInChildren<Text>().text = "Mode: Segmentation";

            is_positive_stroke = true;
            ButtonStrokeType.GetComponent<Button>().GetComponentInChildren<Text>().text = "Stroke: Positive";

            is_gpu = true;
            net.set_mode(1);
            net.set_device(0);
            ButtonCaffeMode.GetComponent<Button>().GetComponentInChildren<Text>().text = "GPU";

            ButtonStrokeType.SetActive(true);
            ButtonCaffeMode.SetActive(true);
            ButtonDoSegmentation.SetActive(true);
            ButtonCancelCurrentStroke.SetActive(true);
            ButtonAutoBound.SetActive(true);
            ButtonManualBound.SetActive(true);
            //Debug.Log(is_positive_stroke);
            //Debug.Log(is_gpu);
        }
    }

    public void toggle_stroke_type()
    {
        if (mode == 1)
        {
            is_positive_stroke = !is_positive_stroke;         
            if (is_positive_stroke)
                ButtonStrokeType.GetComponent<Button>().GetComponentInChildren<Text>().text = "Stroke: Positive";
            else
                ButtonStrokeType.GetComponent<Button>().GetComponentInChildren<Text>().text = "Stroke: Negative";
        }
        //Debug.Log(is_positive_stroke);        
    }

    public void toggle_caffe_mode()
    {
        is_gpu = !is_gpu;
        if (mode == 1)
        {
            if (is_gpu)
            {
                ButtonCaffeMode.GetComponent<Button>().GetComponentInChildren<Text>().text = "GPU";
                net.set_mode(1);
                net.set_device(0);
            }
            else
            {
                ButtonCaffeMode.GetComponent<Button>().GetComponentInChildren<Text>().text = "CPU";
                net.set_mode(0);
            }
        }
        //Debug.Log(is_gpu);
    }

    public void toggle_clear_view()
    {
        is_clear_view = !is_clear_view;
        if (is_clear_view)
        {
            if (mode == 0)
                for (int i = 0; i < textSet.Count; i++)
                    textSet[i].GetComponent<Text>().enabled = false;
            else if (mode == 1)
            {
                draw_seg_rst(false);
                for (int i = 0; i < textSet.Count; i++)
                    textSet[i].GetComponent<Text>().enabled = false;                
            }
        }
        else
        {
            if (mode == 0)
            {
                for (int i = 0; i < textSet.Count; i++)
                    textSet[i].GetComponent<Text>().enabled = true;
            }
            else if (mode == 1)
            {
                //draw_seg_rst();
                draw_seg_rst(false);
                if(!poly_min_quad.is_empty())
                    draw_poly(poly_min_quad, Color.green, false);
                for (int i = 0; i < textSet.Count; i++)
                    textSet[i].GetComponent<Text>().enabled = true;
            }

        }
    }

    public void clear_data()
    {   
        if(mode==0)
        { 
            posClick.Clear();
            posMarker.Clear();
            posWorld.Clear();
            distance.Clear();            
        }
        else if(mode==1)
        {
            posInteraction.Clear();
            negInteraction.Clear();
            currentInteraction.Clear();
            im_pos_interaction.Dispose();
            im_neg_interaction.Dispose();
            im_seg_rst.Dispose();
            im_prob_map.Dispose();
            im_pos_interaction = (new cvMat(input_height, input_width, 6, 1)).multiply_scalar(255.0);
            im_neg_interaction = (new cvMat(input_height, input_width, 6, 1)).multiply_scalar(255.0);
            im_prob_map = new cvMat(input_height, input_width, 5, 0);
            im_seg_rst = new cvMat(input_height, input_width, 0, 0);
            draw_seg = false;
            poly_min_quad.Dispose();
            poly_min_quad = new Polygon();
            poly_contour.Dispose();
            poly_contour = new Polygon();
            line_params.Clear();
            is_manual_bounding = false;
            idxSelSet_manual_bound.Clear();
        }
        if(textSet.Count>0)
            for (int i = textSet.Count - 1; i >= 0; i--)
            {
                Destroy(textSet[i]);
                textSet.RemoveAt(i);
            }
        update_view();
    }
}